# Original Dockerfile example from:
#   https://spinozakotlin.github.io/tutorials/docker
FROM eclipse-temurin:21

# Install maven
# RUN apt-get update
# RUN apt-get install -y maven

WORKDIR /code

# Prepare by downloading dependencies
# ADD pom.xml /code/pom.xml
# ADD maven-settings.xml /code/settings.xml

# RUN ["mvn", "-ssettings.xml", "dependency:resolve"]
# RUN ["mvn", "-ssettings.xml", "verify"]

# Add source, compile and package into a fat jar
# ADD deploy/config.properties /code/deploy/config.properties
# ADD src /code/src
# RUN ["mvn", "-ssettings.xml", "package"]

# The configuration is only required for the tests to pass,
# and when the image is deployed, the properties should come from
# a volume mapped to the host
# RUN ["rm", "deploy/config.properties"]

COPY target/colleague-ad-jar-with-dependencies.jar .

EXPOSE 4567
CMD ["java", "-jar", "colleague-ad-jar-with-dependencies.jar"]
