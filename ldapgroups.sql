use ldapgroups_test;

create table owner_set
(
    id int identity
        primary key,
    name nvarchar(25) not null
)
go

create table owners
(
    id int identity,
    owner_set_id int not null,
    person_id nvarchar(10) not null
)
go

create table rule_data
(
    id int identity
        primary key,
    rule_id int not null,
    param_name nvarchar(255) not null,
    param_value nvarchar(255) not null
)
go

create table rules
(
    id int identity,
    class_name nvarchar(50),
    group_name nvarchar(50),
    group_desc nvarchar(255),
    limit_removal tinyint not null,
    after_hours tinyint not null,
    owner_set_id int,
    allow_empty tinyint not null,
    disabled tinyint not null,
    run_alone tinyint
)
go

create table rule_class
(
    id int identity,
    name nvarchar(50)
)
go

