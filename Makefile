#
# Important, if you get a "unauthorized: authentication required" error from nexus you will need to login again with:
#    docker login nexus.ad.cfcc.edu:18095
#
app_name = colleague-ad
build:
	@docker build --no-cache -t $(app_name) .
tag: build
	@docker tag $(app_name) nexus.ad.cfcc.edu:18095/$(app_name)
push: tag
	@docker push nexus.ad.cfcc.edu:18095/$(app_name)
test:
	@echo "Starting a test docker container on the local host..."
	@docker run -d -p 127.0.0.1:4567:4567/tcp -v $(CURDIR)/deploy:/code/deploy/:ro colleague-ad:latest

.PHONY: build tag push test
