## Synopsis

This project provides a web interface for tools used to maintain the Colleague and Active Directory integration.  Most
of the tools are provided for convenience in updating a user account that has been automatically created from Colleague.

## Requirements

The web portion of this software is built on the Java Spark framework which normally runs as a stand-alone jetty web
server with no requirements beyond Java 7.

## Installation

The project is built with Maven.  All the required libraries will be downloaded from local or remote code repositories.

The standard way to build the package for deployment is to use the Maven release plugin.  Note that when Maven runs the
tests they're in a different target build directory, and so I have started setting an absolute path to the test
configuration properties file using a shell environment variable that is read by the BaseTest class. 

1. mvn release:prepare
2. `export CONFIG_PATH="$(pwd)/deploy/config.properties"`
3. mvn release:perform

The release JAR file will be published to the Maven repository and can be downloaded using the URL in the build output.

An alternate way to build a package for installation is to just run the following Maven commands.

1. From the command line run Maven with the package goal.  For example: `mvn package`
    Note: before you deploy, it is often a good idea to clean up the development directory first: `mvn clean`

2. Copy the JAR file from the `target` subdirectory to a location on your server (ex: `/opt/colleague-ad`).  The one
   labeled "-with-dependencies.jar" is the preferred one to use when running as a stand-alone service.

### Deployment Configuration

1. On the server create a `deploy` subdirectory under the main application directory (ex: `/opt/colleague-ad/deploy`)
   and create a config.properties file.  You can copy the sample from `src/main/resources/config.properties` in the source
   tree.  Fill out all the placeholders with actual settings for your site.
   
### SSL Certificate

If you want to secure the stand-alone web application, you will need to create a keystore file and add two lines to the
config.properties file.

1. Create the keystore, this example uses the DigiCert wildcard certificate.

    `openssl pkcs12 -export -in star_ad_cfcc_edu_crt/star_ad_cfcc_edu.crt -inkey star_ad_cfcc_edu.key -name ad.cfcc.edu -out star_ad_cfcc_edu2.p12`
    
    `keytool -importkeystore -deststorepass PASSWORD_HERE -destkeystore star_ad_cfcc_edu.jks -srckeystore star_ad_cfcc_edu2.p12 -srcstoretype PKCS12`
    
    `keytool -import -alias bundle -trustcacerts -file star_ad_cfcc_edu_crt/DigiCertCA.crt -keystore star_ad_cfcc_edu.jks`

2. Add the keystore path and password to the config file:

    `keystore_filename=deploy/star_ad_cfcc_edu.jks`
    
    `keystore_password=PASSWORD_HERE`

3. After you install the jar file, copy the keystore file to the `deploy` subdirectory.

## Build Environment

I used IntelliJ IDEA to create and build this project.  Eclipse should work as well, but you must follow
instructions available online to set it up for Maven.

### Local Repository

The ColUser library used in this project is stored in a local Nexus repository.  You will need to copy the
sample `maven-settings.xml` file to your home `~/.m2/settings.xml` file.

## Docker

A Dockerfile has been provided to be able to run the project in Docker.  A sample Docker Compose file showing how the
"deploy" directory should be mounted is also provided.

1. Build the java package first with: `mvn package`
2. Build the docker file next either with
   1. Docker Compose: `docker compose build`
   2. Docker: `docker build -t colleague-ad .` 
3. Launch the app for testing with Docker Compose: `docker compose up -d`

To deploy this in production I use the targets in the Makefile to build, tag, and upload the docker files to the local
Nexus repository.  From there I can then run `docker compose pull` on the production server. 

## Tests

The few tests that exist can be kicked off with Maven: `mvn test`

## License

All source code is copyright (C) by Cape Fear Community College.  UniData libraries are used by permission.
