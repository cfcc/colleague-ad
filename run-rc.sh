#! /bin/sh
### BEGIN INIT INFO
# Provides:           colleague-ad
# Required-Start:     $network $remote_fs $syslog
# Required-Stop:      $network $remote_fs $syslog
# Default-Start:      2 3 4 5
# Default-Stop:       0 1 6
# Short-Description:  colleague-ad
# Description:        Colleague and AD web tools
### END INIT INFO
#
# Author:    Jakim Friant <jfriant@cfcc.edu>
#
set -e

if [ -e /lib/lsb/init-functions ]; then
	. /lib/lsb/init-functions
else
	. /etc/init.d/functions
fi

export PATH=/sbin:/usr/sbin:/bin:/usr/bin

case "$1" in
    start)
        /opt/colleague-ad/startup.sh
        ;;
    stop)
        /opt/colleague-ad/shutdown.sh
        ;;
    restart)
        /opt/colleague-ad/startup.sh
        /opt/colleague-ad/shutown.sh
        ;;
    *)
        echo "Usage: /etc/init.d/colleague-ad.sh [start|stop|restart]"
        exit 1
esac

exit 0
