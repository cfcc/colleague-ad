#!/usr/bin/env bash
PID=`ps aux | grep java | grep -v grep | awk ' { print $2 }'`
if [ "z" != "z${PID}" ]
then
    kill ${PID}
    exit 0
else
    echo "colleague-ad PID not found"
    exit 1
fi
