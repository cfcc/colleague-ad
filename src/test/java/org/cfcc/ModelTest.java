package org.cfcc;

import asjava.uniobjects.UniFileException;
import asjava.uniobjects.UniSession;
import asjava.uniobjects.UniSessionException;
import asjava.unirpc.UniRPCConnectionException;
import org.cfcc.ColleagueAd.ChangeUsername;
import org.cfcc.model.PersonPin;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Unit Tests for the data model.
 *
 * Created by jfriant80 on 3/23/16.
 */
public class ModelTest extends BaseTest {
    @Test
    public void connectToDatabase() {
        final String EXPECTED_USERNAME = "datatel01";
        UniSession session;
        String username = null;
        try {
            ChangeUsername instance;
            instance = new ChangeUsername(
                    config_prop.getProperty("host"),
                    config_prop.getProperty("username"),
                    config_prop.getProperty("password"),
                    config_prop.getProperty("TEST_PATH")
            );
            instance.connect();
            session = instance.getSession();
            PersonPin ppin = new PersonPin(session);
            ppin.setId(config_prop.getProperty("TEST_ID"));
            username = ppin.user_id.get();
        } catch (UniSessionException | UniRPCConnectionException | UniFileException e) {
            System.err.println(e.toString());
        }
        assertEquals(EXPECTED_USERNAME, username);
    }
}
