package org.cfcc;

import javaxt.security.ActiveDirectory;
import org.junit.Test;

import javax.naming.NamingException;
import javax.naming.ldap.LdapContext;

public class UserTest extends BaseTest {
    @Test
    public void getUserTest() {
        String username = config_prop.getProperty("username");
        String password = config_prop.getProperty("TEST_DOMAIN_PASSWORD");
        String domain_name = config_prop.getProperty("TEST_DOMAIN_NAME");
        String domain_server = config_prop.getProperty("TEST_DOMAIN_SERVER");
        String test_user = config_prop.getProperty("TEST_USERNAME2");
        String domain_search_base = config_prop.getProperty("domain_search_base");

        try {
            LdapContext conn = ActiveDirectory.getConnection(username, password, domain_name, domain_server);
            ActiveDirectory.User user_rec = ActiveDirectory.getUser(test_user, domain_search_base, conn);
            System.out.println(user_rec);
            assert user_rec != null;
            System.out.println(user_rec.getDescription());
            System.out.println(user_rec.getUserAccountControl());
            System.out.println(user_rec.isAccountDisabled());
        } catch (NamingException e) {
            e.printStackTrace();
        }
    }
}