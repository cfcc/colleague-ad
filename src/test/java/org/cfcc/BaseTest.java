package org.cfcc;

import org.cfcc.ColleagueAd.ChangeUsername;
import org.junit.Before;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.Properties;

public class BaseTest {
    protected Properties config_prop;

    @Before
    public void setUp() throws IOException {
        config_prop = new Properties();
        InputStream input_stream = ChangeUsername.class.getClassLoader().getResourceAsStream("config.properties");
        config_prop.load(input_stream);
        // Now look for additional properties in the deployed path
        String config_path = System.getenv("CONFIG_PATH");
        if (config_path == null) {
            config_path = "deploy/config.properties";
        }
        File config_fd = new File(config_path);
        InputStream input_stream2 = Files.newInputStream(config_fd.toPath());
        config_prop.load(input_stream2);
    }
}
