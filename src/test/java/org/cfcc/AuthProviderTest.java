package org.cfcc;

import javaxt.security.ActiveDirectory;
import org.junit.Test;

import org.cfcc.utility.AuthProvider;

import javax.naming.NamingException;
import javax.naming.ldap.LdapContext;

import static junit.framework.TestCase.assertTrue;

public class AuthProviderTest extends BaseTest {
    @Test
    public void authenticationTest() {
        String username = config_prop.getProperty("TEST_LOGIN_USERNAME");
        String password = config_prop.getProperty("TEST_LOGIN_PASSWORD");
        String domain_name = config_prop.getProperty("TEST_DOMAIN_NAME");
        String domain_server = config_prop.getProperty("TEST_DOMAIN_SERVER");
        String search_base = config_prop.getProperty("domain_search_base");

        AuthProvider auth_prov = new AuthProvider();
        if (auth_prov.validate(username, password, domain_name, domain_server, search_base)) {
            ActiveDirectory.User user = auth_prov.getCurrent_user();
            if (user != null) {
                System.out.println(user.toString());
                boolean found_membership = false;
                String group_names = config_prop.getProperty("TEST_LOGIN_GROUP");
                for (String member : user.getMemberOf()) {
                    System.out.println(member);
                    for (String group_name : group_names.split("[|]")) {
                        if (member.contains(group_name)) {
                            found_membership = true;
                            System.out.println("Found admin group: " + group_name);
                            break;
                        }
                    }
                }
                assertTrue(found_membership);
            } else {
                System.out.println("ERROR searching for " + username + ": No user found");
            }
        }
    }

    @Test
    public void authorizationTest() {
        int sl = 0;
        String domain_username = config_prop.getProperty("username");
        String domain_password = config_prop.getProperty("TEST_DOMAIN_PASSWORD");
        String domain_name = config_prop.getProperty("TEST_DOMAIN_NAME");
        String domain_server = config_prop.getProperty("TEST_DOMAIN_SERVER");
        String test_user = config_prop.getProperty("TEST_USERNAME2");
        String search_base = config_prop.getProperty("domain_search_base");
        try {
            LdapContext conn = ActiveDirectory.getConnection(domain_username, domain_password, domain_name, domain_server);
            ActiveDirectory.User current_user = ActiveDirectory.getUser(test_user, search_base, conn);
            AuthProvider ap = new AuthProvider(current_user);
            sl = ap.getSecurityLevel("CN=UERP");
            assert current_user != null;
            System.out.println("Security level for " + current_user.getCommonName() + " is |" + String.valueOf(sl) + "|");
        } catch (NamingException ex) {
            ex.printStackTrace();
        }
        assertTrue(sl != 0);
    }
}
