package org.cfcc.findperson;

import asjava.uniobjects.UniFileException;
import asjava.uniobjects.UniSessionException;
import org.cfcc.model.ColleagueService;
import org.cfcc.model.Person;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;

public class FindPersonModel extends ColleagueService {
    public FindPersonModel(Properties config_prop) {
        super(config_prop.getProperty("host"), config_prop.getProperty("username"), config_prop.getProperty("password"), config_prop.getProperty("database"));
    }

    public String[] get_person_name(String person_id) {
        String[] name = new String[3];
        Person person = new Person(uSession);
        person.setId(person_id);
        try {
            name[0] = person.last_name.get();
            name[1] = person.first_name.get();
            name[2] = person.middle_name.get();
        } catch (UniFileException | UniSessionException e) {
            name[0] = "ERROR";
            name[1] = e.toString();
            name[2] = person_id;
        }
        return name;
    }

    public ArrayList<HashMap<String, String>> get_person_name_list(ArrayList<String> person_id_list) {
        ArrayList<HashMap<String, String>> result = new ArrayList<>();
        Person person = new Person(uSession);
        for (String person_id: person_id_list) {
            try {
                person.setId(person_id);
                StringBuffer this_where_used = new StringBuffer();
                boolean first = true;
                for (String wu : person.where_used.get()) {
                    if (first) {
                        first = false;
                    } else {
                        this_where_used.append(",");
                    }
                    this_where_used.append(wu.substring(0, 3));
                }
                HashMap<String, String> row = new HashMap<>();
                row.put("id", person_id);
                row.put("name", person.last_name.get() + ", " + person.first_name.get() + " " + person.middle_name.get());
                row.put("where_used", this_where_used.toString());
                result.add(row);
            } catch (UniFileException | UniSessionException e) {
                System.out.println("Error reading |" + person_id + "| " + e.getMessage());
            }
        }
        return result;
    }

    private static String capitalize(String str) {
        if (str == null || str.length() == 0) {
            return "";
        }
        if (str.length() == 1) {
            return str.toUpperCase();
        } else {
            return str.substring(0, 1).toUpperCase() + str.substring(1);
        }
    }

    public ArrayList<HashMap<String, String>> search_for_name(String last_name, String first_name) {
        if (last_name == null && first_name == null) {
            return null;
        } else {
            Person person = new Person(uSession);
            StringBuffer query = new StringBuffer("WITH");
            if (last_name != null) {
                query.append(" LAST.NAME LIKE '").append(capitalize(last_name)).append("...'");
            }
            if (last_name != null && first_name != null) {
                query.append(" AND");
            }
            if (first_name != null) {
                query.append(" FIRST.NAME LIKE '").append(capitalize(first_name)).append("...'");
            }
            query.append(" BY SORT.NAME");
            ArrayList<String> select_result = person.select(query.toString());
            return get_person_name_list(select_result);
        }
    }
}
