package org.cfcc.findperson;

import com.google.gson.Gson;
import org.cfcc.login.LoginController;
import org.cfcc.utility.ConfigProp;
import org.cfcc.utility.StandardResponse;
import org.cfcc.utility.StatusResponse;
import spark.Request;
import spark.Response;
import spark.Route;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class FindPersonController {
    public static Route fetchOnePerson = (Request request, Response response) -> {
        response.type("application/json");
        Map<String, Object> attributes = LoginController.check_login(request, response);
        if ((int) attributes.getOrDefault("securityLevel", 0) >= 0) {
            HashMap<String, String> result = new HashMap<>();
            String person_id = request.params(":id");
            Properties config_prop = ConfigProp.load();
            FindPersonModel instance = new FindPersonModel(config_prop);
            instance.connect();
            String[] person_name = instance.get_person_name(person_id);
            result.put("person_first", person_name[1]);
            result.put("person_last", person_name[0]);
            return new Gson().toJson(new StandardResponse(StatusResponse.SUCCESS, new Gson().toJsonTree(result)));
        } else {
            String msg = "User " +
                    attributes.getOrDefault("username", "UNKNOWN") +
                    " (securityLevel: " +
                    attributes.getOrDefault("securityLevel", "?") +
                    ") is not authorized";
            System.err.println(msg);
            return new Gson().toJson(new StandardResponse(StatusResponse.ERROR, "Not Authorized"));
        }
    };

    public static Route findByName = (Request request, Response response) -> {
        response.type("application/json");
        Map<String, Object> attributes = LoginController.check_login(request, response);
        if ((int) attributes.getOrDefault("securityLevel", 0) >= 0) {
            // get the first and last name fields from the request
            String last_name = request.queryParams("last");
            String first_name = request.queryParams("first");
            Properties config_prop = ConfigProp.load();
            FindPersonModel instance = new FindPersonModel(config_prop);
            instance.connect();
            ArrayList<HashMap<String, String>> result = instance.search_for_name(last_name, first_name);
            if (result != null) {
                return new Gson().toJson(new StandardResponse(StatusResponse.SUCCESS, new Gson().toJsonTree(result)));
            } else {
                return new Gson().toJson(new StandardResponse(StatusResponse.ERROR, "No records found."));
            }
        } else {
            String msg = "User " +
                    attributes.getOrDefault("username", "UNKNOWN") +
                    " (securityLevel: " +
                    attributes.getOrDefault("securityLevel", "?") +
                    ") is not authorized";
            System.err.println(msg);
            return new Gson().toJson(new StandardResponse(StatusResponse.ERROR, "Not Authorized"));
        }
    };
}
