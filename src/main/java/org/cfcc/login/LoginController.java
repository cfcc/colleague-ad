package org.cfcc.login;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.cfcc.utility.AuthProvider;
import org.cfcc.utility.ConfigProp;

import spark.*;

public class LoginController {
    public static TemplateViewRoute serveLoginPage = (Request request, Response response) -> {
        Map<String, Object> attributes = new HashMap<>();
        Session session = request.session();
        if (session.attribute("error") != null) {
            attributes.put("error", session.attribute("error"));
            session.removeAttribute("error");
        }
        // note that the login page does not use the main layout
        return new ModelAndView(attributes, "login.ftl");
    };

    public static Route handleLoginPost = (Request request, Response response) -> {
        String username = request.queryParams("login_username");
        String password = request.queryParams("login_password");
        String redirect_path;
        Properties config_prop = ConfigProp.load();
        String domain_name = config_prop.getProperty("domain_name", "");
        String domain_server = config_prop.getProperty("domain_server", "");
        String search_base = config_prop.getProperty("domain_search_base", "");
        Session session = request.session(true);
        AuthProvider auth_prov = new AuthProvider();
        boolean result = auth_prov.validate(username, password, domain_name, domain_server, search_base);
        if (result) {
            int security_level = auth_prov.getSecurityLevel(config_prop.getProperty("admin_group", "NONE"));
            session.attribute("username", username);
            session.attribute("securityLevel", security_level);
            session.attribute("message", "You have been signed in");
            //System.out.println("[DEBUG] " + username + " logged in with securityLevel=" + Integer.toString(security_level));
            if (session.attribute("loginRedirect") != null) {
                redirect_path = session.attribute("loginRedirect");
                session.removeAttribute("loginRedirect");
            } else {
                redirect_path = "/";
            }
            response.redirect(redirect_path);
        } else {
            session.removeAttribute("username");
            session.attribute("securityLevel", 0);
            session.attribute("error", "Invalid username or password");
            response.redirect("/login");
        }
        return null;
    };

    public static Route handleLogoutPost = (Request request, Response response) -> {
        Session session = request.session();
        session.removeAttribute("username");
        session.attribute("securityLevel", 0);
        session.attribute("message", "You have been signed out");
        response.redirect("/");
        return null;
    };

    // the origin of the request is saved in the session so the user can be redirected back after login
    public static void ensureUserIsLoggedIn(Request request, Response response) {
        if (request.session().attribute("username") == null) {
            request.session().attribute("loginRedirect", request.pathInfo());
            response.redirect("/login");
        }
    }

    public static Map<String, Object> check_login(Request request, Response response) {
        return check_login_no_redirect(request, response, false);
    }

    public static Map<String, Object> check_login_no_redirect(Request request, Response response, boolean no_redirect) {
        Map<String, Object> view_objects = new HashMap<>();
        Session session = request.session();
        if (session.attribute("username") == null) {
            if (!no_redirect) {
                session.attribute("loginRedirect", request.pathInfo());
                response.redirect("/login");
            }
        } else {
            view_objects.put("logged_in", "YES");
        }
        if (session.attribute("securityLevel") != null) {
            view_objects.put("securityLevel", session.attribute("securityLevel"));
        } else {
            String username = session.attribute("username");
            if (username != null) {
                System.err.println("Failed to find security level for " + username);
            }
            view_objects.put("securityLevel", 0);
        }
        if (session.attribute("message") != null) {
            view_objects.put("message", session.attribute("message"));
            session.removeAttribute("message");
        }
        return view_objects;
    }

}
