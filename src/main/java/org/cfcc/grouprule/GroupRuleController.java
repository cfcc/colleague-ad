package org.cfcc.grouprule;

import com.google.gson.Gson;
import org.cfcc.login.LoginController;
import org.cfcc.utility.StandardResponse;
import org.cfcc.utility.StatusResponse;
import org.cfcc.ownerset.OwnerSetModel;
import org.cfcc.utility.ConfigProp;
import spark.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class GroupRuleController {
    public static TemplateViewRoute fetchAllGroupRules = (Request request, Response response) -> {
        Map<String, Object> attributes = new HashMap<>();

        Properties config_prop = ConfigProp.load();

        GroupRuleModel instance = new GroupRuleModel(config_prop);
        OwnerSetModel instance2 = new OwnerSetModel(config_prop);
        attributes.put("group_list", instance.get_all());
        attributes.put("owner_sets", instance2.get_all_owner_sets());

        /* This autocomplete code came from https://www.w3schools.com/howto/howto_js_autocomplete.asp */
        attributes.put("page_javascript", "/js/w3s_autocomplete.js");
        attributes.put("page_stylesheet", "/css/w3s_autocomplete.css");

        attributes.put("rule_class_names", instance.get_rule_class_names());

        attributes.put("templateName", "group_list.ftl");
        attributes.putAll(LoginController.check_login(request, response));

        return new ModelAndView(attributes, "layout.ftl");
    };

    public static Route fetchOneGroupRule = (Request req, Response res)->{
        res.type("application/json");
        Map<String, Object> attributes = LoginController.check_login(req, res);
        if ((int) attributes.getOrDefault("securityLevel", 0) >= 0) {
            Properties config_prop = ConfigProp.load();
            GroupRuleModel instance = new GroupRuleModel(config_prop);
            HashMap<String, Object> result = instance.get_rule_definition(req.params(":id"));
            return new Gson().toJson(new StandardResponse(StatusResponse.SUCCESS, new Gson().toJsonTree(result)));
        } else {
            String msg = "User " +
                    attributes.getOrDefault("username", "UNKNOWN") +
                    " (securityLevel: " +
                    attributes.getOrDefault("securityLevel", "?") +
                    ") is not authorized";
            System.err.println(msg);
            res.status(401);
            return new Gson().toJson(new StandardResponse(StatusResponse.ERROR, "Not Authorized"));
        }
    };

    public static Route saveGroupRule = (Request request, Response response) -> {
        response.type("application/json");
        Map<String, Object> attributes = LoginController.check_login(request, response);
        if ((int) attributes.getOrDefault("securityLevel", 0) >= 3) {
            Properties config_prop = ConfigProp.load();
            GroupRuleModel instance = new GroupRuleModel(config_prop);
            HashMap<String, String> form_data = new HashMap<>();
            for(String key: request.queryParams()) {
                // System.out.println("[DEBUG] saveGroupRule() " + key + ": " + request.queryParams(key));
                form_data.put(key, request.queryParams(key));
            }
            form_data.put("id", request.params(":id"));
            if (instance.save_group_rule(form_data)) {
                return new Gson().toJson(new StandardResponse(StatusResponse.SUCCESS, "Record updated!"));
            } else {
                // FIXME: I need to be able to splash the message up on the page
                response.status(500);
                return new Gson().toJson(new StandardResponse(StatusResponse.ERROR, "Error saving record"));
            }
        } else {
            String msg = "User " +
                    attributes.getOrDefault("username", "UNKNOWN") +
                    " (securityLevel: " +
                    attributes.getOrDefault("securityLevel", "?") +
                    ") is not authorized";
            System.err.println(msg);
            response.status(401);
            return new Gson().toJson(new StandardResponse(StatusResponse.ERROR, "Not Authorized"));
        }
    };

    public static Route deleteGroupRule = (Request request, Response response) -> {
        response.type("application/json");
        Map<String, Object> attributes = LoginController.check_login(request, response);
        if ((int) attributes.getOrDefault("securityLevel", 0) >= 3) {
            int rule_id = Integer.parseInt(request.params(":id"));
            Properties config_prop = ConfigProp.load();
            GroupRuleModel instance = new GroupRuleModel(config_prop);
            if (instance.delete_group_rule(rule_id)) {
                return new Gson().toJson(new StandardResponse(StatusResponse.SUCCESS, "Deleted group rule record" + rule_id));
            } else {
                response.status(500);
                return new Gson().toJson(new StandardResponse(StatusResponse.ERROR, "Failed to delete group rule record " + rule_id));
            }
        } else {
            String msg = "User " +
                    attributes.getOrDefault("username", "UNKNOWN") +
                    " (securityLevel: " +
                    attributes.getOrDefault("securityLevel", "?") +
                    ") is not authorized";
            System.err.println(msg);
            response.status(401);
            return new Gson().toJson(new StandardResponse(StatusResponse.ERROR, "Not Authorized"));
        }
    };
}
