package org.cfcc.grouprule;

import com.microsoft.sqlserver.jdbc.SQLServerException;
import org.cfcc.model.SQLService;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;

public class GroupRuleModel extends SQLService {

    GroupRuleModel(String username, String password, String hostname, String db_name) {
        super(username, password, hostname, db_name);
    }

    public GroupRuleModel(Properties config_prop) {
        super(config_prop.getProperty("sql_username"),
                config_prop.getProperty("sql_password"),
                config_prop.getProperty("sql_hostname"),
                config_prop.getProperty("sql_db_name"));
    }

    public ArrayList<HashMap<String, String>> get_all() {
        ArrayList<HashMap<String, String>> result = new ArrayList<>();
        String query = "SELECT rules.id, class_name, group_name, group_desc, disabled, after_hours, run_alone, owner_set_id, owner_set.name as owners_name FROM rules LEFT JOIN owner_set on owner_set.id = owner_set_id ORDER BY class_name, group_name, group_desc";
        try {
            Connection con = ds.getConnection();
            CallableStatement cstmt = con.prepareCall(query);
            ResultSet rs = cstmt.executeQuery();
            while (rs.next()) {
                HashMap<String, String> row = new HashMap<>();
                row.put("id", rs.getString("id"));
                row.put("class_name", rs.getString("class_name"));
                row.put("group_name", rs.getString("group_name"));
                row.put("group_desc", rs.getString("group_desc"));
                row.put("disabled", rs.getString("disabled"));
                row.put("after_hours", rs.getString("after_hours"));
                row.put("run_alone", rs.getString("run_alone"));
                String owners_id = rs.getString("owner_set_id");
                if (owners_id == null) {
                    row.put("owner_set_id", "0");
                    row.put("owners_name", "Add Owners");
                } else {
                    row.put("owner_set_id", rs.getString("owner_set_id"));
                    row.put("owners_name", rs.getString("owners_name"));
                }
                result.add(row);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return result;
    }

    public String get_rule_class_names() {
        StringBuffer result = new StringBuffer("['");
        String query = "SELECT name FROM rule_class ORDER BY name";
        try {
            Connection con = ds.getConnection();
            CallableStatement cstmt = con.prepareCall(query);
            ResultSet rs = cstmt.executeQuery();
            boolean first = true;
            while (rs.next()) {
                if (first) {
                    first = false;
                    result.append(rs.getString("name"));
                } else {
                    result.append("','").append(rs.getString("name"));
                }
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        result.append("']");
        return result.toString();
    }

    public HashMap<String, Object> get_rule_definition(String rule_id) {
        String[] fields = {"class_name", "group_name", "group_desc", "limit_removal", "after_hours", "allow_empty", "disabled", "run_alone", "owner_set_id"};
        String query = "SELECT " + String.join(",", fields) + " FROM rules WHERE id = ?";
        String query2 = "SELECT id, param_name, param_value FROM rule_data WHERE rule_id = ?";

        HashMap<String, Object> row = new HashMap<>();
        // Initializing every default field to a blank string
        for (String k : fields) {
            row.put(k, "");
        }
        try {
            Connection con = ds.getConnection();
            CallableStatement cstmt = con.prepareCall(query);
            cstmt.setString(1, rule_id);
            ResultSet rs = cstmt.executeQuery();
            if (rs.next()) {
                for (String name : fields) {
                    String value = rs.getString(name);
                    if (value == null) {
                        value = "";
                    }
                    row.put(name, value);
                }
            }
            CallableStatement cstmt2 = con.prepareCall(query2);
            cstmt2.setString(1, rule_id);
            ResultSet rs2 = cstmt2.executeQuery();
            ArrayList<HashMap<String, String>> extra = new ArrayList<>();
            while (rs2.next()) {
                HashMap<String, String> rule_data = new HashMap<>();
                rule_data.put("id", rs2.getString("id"));
                rule_data.put("param_name", rs2.getString("param_name"));
                rule_data.put("param_value", rs2.getString("param_value"));
                extra.add(rule_data);
            }
            row.put("rule_data", extra);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            System.err.println(query);
        }
        return row;
    }

    public boolean save_group_rule(HashMap<String, String> form_data) {
        boolean success = true;
        HashMap<String, Object> db_values = new HashMap<>();
        try {
            Connection con = ds.getConnection();
            // parse the incoming data so it will be appropriate for the SQL fields
            for (String key : new String[]{"limit_removal", "after_hours", "allow_empty", "disabled", "run_alone"}) {
                if (form_data.containsKey(key)) {
                    db_values.put(key, 1);
                } else {
                    db_values.put(key, 0);
                }
            }
            for (String key : new String[]{"class_name", "group_name", "group_desc"}) {
                if (form_data.containsKey(key)) {
                    if (form_data.get(key).equals("")) {
                        db_values.put(key, null);
                    } else {
                        db_values.put(key, form_data.get(key));
                    }
                } else {
                    db_values.put(key, null);
                }
            }
            //for (String key : db_values.keySet()) {
            //    System.out.println("[DEBUG] save_group_rule() " + key + ": " + db_values.get(key));
            //}
            int record_id;
            if (form_data.containsKey("id")) {
                try {
                    record_id = Integer.parseInt(form_data.get("id"));
                } catch (NumberFormatException e) {
                    record_id = 0;
                }
            } else {
                record_id = 0;
            }

            // Update an existing record if we have an ID, otherwise create a new record.
            if (record_id > 0) {
                String query = "UPDATE rules SET class_name = ?, group_name = ?, group_desc = ?, limit_removal = ?, after_hours = ?, allow_empty = ?, disabled = ?, run_alone = ? WHERE id = ?";
                db_values.put("id", Integer.parseInt(form_data.get("id")));
                try {
                    PreparedStatement ps = con.prepareStatement(query);
                    ps.setString(1, (String) db_values.get("class_name"));
                    ps.setString(2, (String) db_values.get("group_name"));
                    ps.setString(3, (String) db_values.get("group_desc"));
                    ps.setInt(4, (Integer) db_values.get("limit_removal"));
                    ps.setInt(5, (Integer) db_values.get("after_hours"));
                    ps.setInt(6, (Integer) db_values.get("allow_empty"));
                    ps.setInt(7, (Integer) db_values.get("disabled"));
                    ps.setInt(8, (Integer) db_values.get("run_alone"));
                    ps.setInt(9, (Integer) db_values.get("id"));
                    int affected_rows = ps.executeUpdate();
                    if (affected_rows == 0) {
                        throw new SQLException("Failed to update record");
                    }
                } catch (SQLException throwables) {
                    success = false;
                    throwables.printStackTrace();
                }
            } else {
                String query = "INSERT INTO rules (class_name, group_name, group_desc, limit_removal, after_hours, allow_empty, disabled, run_alone) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
                try {
                    PreparedStatement ps = con.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
                    ps.setString(1, (String) db_values.get("class_name"));
                    ps.setString(2, (String) db_values.get("group_name"));
                    ps.setString(3, (String) db_values.get("group_desc"));
                    ps.setInt(4, (Integer) db_values.get("limit_removal"));
                    ps.setInt(5, (Integer) db_values.get("after_hours"));
                    ps.setInt(6, (Integer) db_values.get("allow_empty"));
                    ps.setInt(7, (Integer) db_values.get("disabled"));
                    ps.setInt(8, (Integer) db_values.get("run_alone"));
                    int affected_rows = ps.executeUpdate();
                    if (affected_rows == 0) {
                        throw new SQLException("Failed to insert record");
                    }
                    try (ResultSet generated_keys = ps.getGeneratedKeys()) {
                        if (generated_keys.next()) {
                            db_values.put("id", generated_keys.getInt(1));
                        } else {
                            throw new SQLException("Failed to create record, no ID obtained");
                        }
                    }
                } catch (SQLException throwables) {
                    success = false;
                    throwables.printStackTrace();
                }
            }

            if (success) {
                // Owner Set is handled separately because we'll set it to null if the user removes it
                if (form_data.containsKey("owner_set_id")) {
                    try {
                        String query2 = "UPDATE rules SET owner_set_id = ? WHERE id = ?";
                        PreparedStatement ps2 = con.prepareStatement(query2);
                        int owner_set_id = Integer.parseInt((String) form_data.get("owner_set_id"));
                        if (owner_set_id > 0) {
                            ps2.setInt(1, owner_set_id);
                        } else {
                            ps2.setNull(1, Types.INTEGER);
                        }
                        ps2.setInt(2, (Integer) db_values.get("id"));
                        ps2.executeUpdate();
                        ps2.close();
                    } catch (SQLException throwables) {
                        success = false;
                        throwables.printStackTrace();
                    }
                }
            }

            if (success) {
                // Now check for additional rule_data entries that need updating or creating
                HashMap<Integer, HashMap<String, String>> rule_data = new HashMap<>();
                form_data.forEach((K, V) -> {
                    if (K.startsWith("rule_data_")) {
                        int my_index = Integer.parseInt(K.substring(K.indexOf("[") + 1, K.indexOf("]")));
                        String my_key = "param_" + K.substring(10, K.indexOf("["));
                        HashMap<String, String> param_data;
                        if (rule_data.containsKey(my_index)) {
                            param_data = rule_data.get(my_index);
                        } else {
                            param_data = new HashMap<>();
                        }
                        param_data.put(my_key, V);
                        rule_data.put(my_index, param_data);
                    }
                });
                for (int my_index : rule_data.keySet()) {
                    HashMap<String, String> param_data = rule_data.get(my_index);
                    try {
                        String query3;
                        if (param_data.get("param_name").isEmpty() && param_data.get("param_value").isEmpty()) {
                            //System.out.println("[DEBUG] save_group_rule() Delete " + param_data);
                            query3 = "DELETE FROM rule_data WHERE id = ?";
                            PreparedStatement ps3 = con.prepareStatement(query3);
                            ps3.setInt(1, my_index);
                            ps3.executeUpdate();
                            ps3.close();
                        } else {
                            if (my_index > 0) {
                                //System.out.println("[DEBUG] save_group_rule() Update " + my_index + ": " + param_data);
                                query3 = "UPDATE rule_data SET rule_id = ?, param_name = ?, param_value = ? WHERE id = ?";
                                PreparedStatement ps3 = con.prepareStatement(query3);
                                ps3.setInt(1, (Integer) db_values.get("id"));
                                ps3.setString(2, param_data.get("param_name"));
                                ps3.setString(3, param_data.get("param_value"));
                                ps3.setInt(4, my_index);
                                ps3.executeUpdate();
                                ps3.close();
                            } else {
                                //System.out.println("[DEBUG] save_group_rule() Insert " + param_data);
                                query3 = "INSERT INTO rule_data (rule_id, param_name, param_value) VALUES (?, ?, ?)";
                                PreparedStatement ps3 = con.prepareStatement(query3);
                                ps3.setInt(1, (Integer) db_values.get("id"));
                                ps3.setString(2, param_data.get("param_name"));
                                ps3.setString(3, param_data.get("param_value"));
                                ps3.executeUpdate();
                                ps3.close();
                            }
                        }
                    } catch (SQLException throwables) {
                        throwables.printStackTrace();
                    }
                }
            }
        } catch (SQLServerException throwables) {
            throwables.printStackTrace();
        }
        return success;
    }

    public boolean delete_group_rule(int rule_id) {
        boolean success = false;
        String query1 = "DELETE FROM rule_data WHERE rule_id = ?";
        String query2 = "DELETE FROM rules WHERE id = ?";
        try {
            Connection con = ds.getConnection();

            PreparedStatement ps1 = con.prepareStatement(query1);
            ps1.setInt(1, rule_id);
            ps1.executeUpdate();

            PreparedStatement ps2 = con.prepareStatement(query2);
            ps2.setInt(1, rule_id);
            ps2.executeUpdate();

            success = true;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return success;
    }
}
