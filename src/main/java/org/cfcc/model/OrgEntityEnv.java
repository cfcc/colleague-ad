package org.cfcc.model;

import asjava.uniobjects.UniSession;
import org.cfcc.colfile.StringColumn;
import org.cfcc.colfile.Table;

/**
 * Created by jfriant80 on 3/22/16.
 */
public class OrgEntityEnv extends Table {
    public StringColumn oee_username;

    public OrgEntityEnv(UniSession s) {
        super(s, "ORG.ENTITY.ENV");
        oee_username = new StringColumn(this, 17);
    }
}
