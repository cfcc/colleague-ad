package org.cfcc.model;

import asjava.uniobjects.UniSession;
import org.cfcc.colfile.StringColumn;
import org.cfcc.colfile.Table;

/**
 * Created by jfriant80 on 3/22/16.
 */
public class OrgEntity extends Table {

    public StringColumn oe_last_name;
    public StringColumn oe_first_name;
    public StringColumn oe_middle_name;
    public StringColumn oe_org_entity_env;

    public OrgEntity(UniSession session) {
        super(session, "ORG.ENTITY");
        oe_last_name = new StringColumn(this, 3);
        oe_first_name = new StringColumn(this, 4);
        oe_middle_name = new StringColumn(this, 5);
        oe_org_entity_env = new StringColumn(this, 14);
    }
}
