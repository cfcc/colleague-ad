package org.cfcc.model;

import asjava.uniobjects.UniSession;
import org.cfcc.colfile.ListColumn;
import org.cfcc.colfile.StringColumn;
import org.cfcc.colfile.Table;
import org.cfcc.colfile.TransColumn;

public class Applicants extends Table {
    public ListColumn app_applications;
    public StringColumn applicants_adddate;
    public TransColumn app_start_terms;

    public Applicants(UniSession session) {
        super(session, "APPLICANTS");
        app_applications = new ListColumn(this, 18);
        app_start_terms = new TransColumn(this, 18, "APPLICATIONS", 9);
        applicants_adddate = new StringColumn(this, 24, "D2/MDY");
    }
}
