package org.cfcc.model;

import asjava.uniobjects.UniSession;
import org.cfcc.colfile.StringColumn;
import org.cfcc.colfile.Table;

public class Terms extends Table {
    public StringColumn term_start_date;
    public StringColumn term_end_date;

    public Terms(UniSession s) {
        super(s, "TERMS");

        term_start_date = new StringColumn(this, 5, "D2/MDY");
        term_end_date = new StringColumn(this, 6, "D2/MDY");
    }
}
