package org.cfcc.model;

import asjava.uniobjects.UniSession;
import org.cfcc.colfile.ListColumn;
import org.cfcc.colfile.Table;

public class StudentTerms extends Table {
    public ListColumn sttr_status;

    public StudentTerms(UniSession session) {
        super(session, "STUDENT.TERMS");
        sttr_status = new ListColumn(this, 15);
    }
}
