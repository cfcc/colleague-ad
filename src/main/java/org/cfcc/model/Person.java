package org.cfcc.model;

import asjava.uniobjects.UniSession;
import org.cfcc.colfile.ListColumn;
import org.cfcc.colfile.StringColumn;
import org.cfcc.colfile.Table;

public class Person extends Table {
    public StringColumn last_name;
    public StringColumn first_name;
    public StringColumn middle_name;
    public ListColumn where_used;
    public StringColumn privacy_flag;

    /**
     * Constructor to bind to an open session and initialize the field variables
     *
     * @param session a current UniObjects session
     */
    public Person(UniSession session) {
        super(session, "PERSON");
        last_name = new StringColumn(this, 1);
        first_name = new StringColumn(this, 3);
        middle_name = new StringColumn(this, 4);
        privacy_flag = new StringColumn(this, 186);
        where_used = new ListColumn(this, 31);
    }
}
