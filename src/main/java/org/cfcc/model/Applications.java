package org.cfcc.model;

import asjava.uniobjects.UniSession;
import org.cfcc.colfile.ListColumn;
import org.cfcc.colfile.Table;

public class Applications extends Table {
    public ListColumn appl_applicant;
    public ListColumn appl_acad_program;
    public ListColumn appl_no;
    public ListColumn appl_date;
    public ListColumn appl_status;
    public ListColumn appl_status_date;
    public ListColumn appl_start_term;

    public Applications(UniSession session) {
        super(session, "APPLICATIONS");

        appl_applicant = new ListColumn(this, 1);
        appl_acad_program = new ListColumn(this, 2);
        appl_no = new ListColumn(this, 3);
        appl_date = new ListColumn(this, 4, "D2/MDY");
        appl_status = new ListColumn(this, 5);
        appl_status_date = new ListColumn(this, 6, "D2/MDY");
        appl_start_term = new ListColumn(this, 9);
    }
}
