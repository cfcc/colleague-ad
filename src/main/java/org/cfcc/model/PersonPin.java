package org.cfcc.model;

import asjava.uniobjects.UniSession;
import org.cfcc.colfile.StringColumn;
import org.cfcc.colfile.Table;

/**
 * Created by jfriant80 on 3/22/16.
 */
public class PersonPin extends Table {
    /** This field holds the username */
    public StringColumn user_id;

    /**
     * Constructor to bind to an open session and initialize the field variables
     *
     * @param session a current UniObjects session
     */
    public PersonPin(UniSession session) {
        super(session, "PERSON.PIN");
        user_id = new StringColumn(this, 8);
    }
}
