package org.cfcc.model;

import com.microsoft.sqlserver.jdbc.SQLServerDataSource;

public class SQLService {
    protected SQLServerDataSource ds;

    public SQLService(String username, String password, String hostname, String db_name) {
        ds = new SQLServerDataSource();
        ds.setUser(username);
        ds.setPassword(password);
        ds.setServerName(hostname);
        ds.setDatabaseName(db_name);
        ds.setSSLProtocol("TLSv1.2");
    }

    public SQLService(String username, String password, String hostname, String db_name, String ssl_protocol) {
        ds = new SQLServerDataSource();
        ds.setUser(username);
        ds.setPassword(password);
        ds.setServerName(hostname);
        ds.setDatabaseName(db_name);
        ds.setSSLProtocol(ssl_protocol);
    }
}
