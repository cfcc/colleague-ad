package org.cfcc.model;

import asjava.uniobjects.UniFileException;
import asjava.uniobjects.UniSession;
import asjava.uniobjects.UniSessionException;
import org.cfcc.colfile.ListColumn;
import org.cfcc.colfile.Table;

import java.util.ArrayList;

public class StValcodes extends Table {
    private ListColumn val_minimum_input_string;
    private ListColumn val_external_representation;

    public StValcodes(UniSession s) {
        super(s, "ST.VALCODES");
        val_minimum_input_string = new ListColumn(this, 1);
        val_external_representation = new ListColumn(this, 2);
    }

    public String getDescription(String code) {
        try {
            ArrayList<String> codes = new ArrayList<String>(this.val_minimum_input_string.get());
            ArrayList<String> descriptions = new ArrayList<String>(this.val_external_representation.get());
            for (int i = 0; i < codes.size(); i++) {
                if (codes.get(i).equals(code)) {
                    return descriptions.get(i);
                }
            }
        } catch (UniFileException | UniSessionException e) {
            e.printStackTrace();
        }
        return "NULL";
    }
}
