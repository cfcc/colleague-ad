package org.cfcc.model;

import asjava.uniobjects.UniSession;
import org.cfcc.colfile.ListColumn;
import org.cfcc.colfile.Table;

/*
 * Note that this table is unique in that only one record, SCHEDULED.PHANTOMS, actually has the data, the rest of the
 * records are the paragraphs.
 *
 * For example:
 *     LIST SCHEDULED.PHANTOMS 'SCHEDULED.PHANTOMS' SCPH.PARAGRAPH.ID SCPH.NEXT.RUN.DATE SCPH.NEXT.RUN.TIME
 */

public class ScheduledPhantoms extends Table {
    public ListColumn scph_paragraph_id;
    public ListColumn scph_next_run_date;
    public ListColumn scph_next_run_time;
    public ListColumn scph_sched_start_date;
    public ListColumn scph_sched_run_time;
    public ListColumn scph_sched_frequency;
    public ListColumn scph_sched_interval;
    public ListColumn scph_sched_stop_date;
    public ListColumn scph_weekday_only_flag;
    public ListColumn scph_sched_from;

    public ScheduledPhantoms(UniSession session) {
        super(session, "SCHEDULED.PHANTOMS");

        scph_paragraph_id = new ListColumn(this, 1);
        scph_next_run_date = new ListColumn(this, 2, "D4/MDY");
        scph_next_run_time = new ListColumn(this, 3, "MTH");
        scph_sched_start_date = new ListColumn(this, 4, "D2/MDY");
        scph_sched_run_time = new ListColumn(this, 5, "MTH");
        scph_sched_frequency = new ListColumn(this, 6);
        scph_sched_interval = new ListColumn(this, 7);
        scph_sched_stop_date = new ListColumn(this, 8, "D2/MDY");
        scph_weekday_only_flag = new ListColumn(this, 9);
        scph_sched_from = new ListColumn(this, 10);
    }
}
