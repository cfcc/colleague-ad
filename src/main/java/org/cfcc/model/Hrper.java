package org.cfcc.model;

import asjava.uniobjects.UniSession;
import org.cfcc.colfile.ListColumn;
import org.cfcc.colfile.Table;

public class Hrper extends Table {
    public ListColumn hrp_perpos_start_date;
    public ListColumn hrp_perpos_end_date;
    public ListColumn hrp_perpos_position_id;

    public Hrper(UniSession session) {
        super(session, "HRPER");
        hrp_perpos_start_date = new ListColumn(this, 5, "D2/MDY");
        hrp_perpos_end_date = new ListColumn(this, 6, "D2/MDY");
        hrp_perpos_position_id = new ListColumn(this, 8);
    }
}
