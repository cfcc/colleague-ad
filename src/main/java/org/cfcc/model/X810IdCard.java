package org.cfcc.model;

import asjava.uniobjects.UniSession;
import org.cfcc.colfile.StringColumn;
import org.cfcc.colfile.Table;

public class X810IdCard extends Table {
    public StringColumn x810_id_ldap_end_date;

    public X810IdCard(UniSession session) {
        super(session, "X810.ID.CARD");
        x810_id_ldap_end_date = new StringColumn(this, 42, "D4/MDY");
    }
}
