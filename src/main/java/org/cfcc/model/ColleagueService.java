package org.cfcc.model;

import asjava.uniobjects.UniSession;
import asjava.uniobjects.UniSessionException;
import asjava.unirpc.UniRPCConnectionException;

public class ColleagueService {
    private static final int RPC_TIMEOUT = 300;

    protected UniSession uSession;

    private String server_address;
    private String username;
    private String password;
    private String account_path;

    public ColleagueService() {
        server_address = null;
        username = null;
        password = null;
        account_path = null;
    }

    public ColleagueService(String server_address, String username, String password, String account_path) {
        this.server_address = server_address;
        this.username = username;
        this.password = password;
        this.account_path = account_path;
    }

    public void connect()
            throws UniSessionException, UniRPCConnectionException
    {
        uSession = new UniSession();

        // set up the connection information
        uSession.setHostName(server_address);
        uSession.setUserName(username);
        uSession.setPassword(password);
        uSession.setAccountPath(account_path);

        // This seems to only set the timeout on commands sent after the
        // connection has been established.
        uSession.setTimeout(RPC_TIMEOUT);

        // This appears to be working to limit the amount of time waiting
        // for a connection.
        uSession.connection.setTimeoutSeconds(RPC_TIMEOUT);

        // connect to the server
        uSession.connect();
    }

    public UniSession getSession() {
        return uSession;
    }
}
