package org.cfcc.model;

import asjava.uniobjects.UniSession;
import org.cfcc.colfile.ListColumn;
import org.cfcc.colfile.ComputedColumn;
import org.cfcc.colfile.Table;

public class Students extends Table {
    public ListColumn stu_terms;
    public ComputedColumn stu_current_type;

    public Students(UniSession session) {
        super(session, "STUDENTS");
        stu_terms = new ListColumn(this, 23);
        stu_current_type = new ComputedColumn(this, "STU.CURRENT.TYPE");
    }
}
