package org.cfcc.utility;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import static spark.Spark.halt;

public class ConfigProp {
    public static Properties load() {
        Properties config_prop = new Properties();
        // Load the default properties built in to the JAR file
        try {
            InputStream input_stream = ConfigProp.class.getClassLoader().getResourceAsStream("config.properties");
            if (input_stream != null) {
                config_prop.load(input_stream);
            }
        } catch (IOException e) {
            halt(500, "Failed to open config.properties");
        }
        // Now look for additional properties in the deployed path
        try {
            File config_fd = new File("deploy/config.properties");
            InputStream input_stream = new FileInputStream(config_fd);
            config_prop.load(input_stream);
        } catch (IOException ignored) {
            System.err.println("File not found: deploy/config.properties");
        }
        return config_prop;
    }
}
