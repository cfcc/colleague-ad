package org.cfcc.utility;

/**
 * Custom exception class thrown when the password cannot be saved to LDAP.
 * This can happen when either the server has crashed or if it is in read-only
 * mode.
 * <p>
 * When this exception is caught, the user should be notified that the service
 * is unavaiable and to try again later.
 */
public class LdapChangeException extends Exception {
    /**
     *
     */
    private static final long serialVersionUID = 6710621674729483062L;
    /** holds the DN for which the error occured */
    public String dn = "";
    /** holds the reason for the error (in addition to the LDAP message) */
    public String msg = "";

    /**
     * Class default constructor
     *
     * @param s  the error message
     */
    public LdapChangeException(String s) {
        super(s);
    }

    /**
     * Class constructor to store the last DN that had the error.
     *
     * @param error_dn  the dn that had the error
     * @param error_msg any additional error messages
     */
    public LdapChangeException(String error_dn, String error_msg) {
        msg = error_msg;
        dn = error_dn;
    }

    public LdapChangeException(String msg, Throwable ex) {
        super(msg, ex);
    }

    /**
     * Returns a custom error message with the DN, reason, and LDAP error
     * message.
     *
     * @return the string with the error message
     */
    @Override
    public String toString() {
        return "Error updating LDAP for " + dn + ", " + msg + ": " + super.getMessage();
    }
}
