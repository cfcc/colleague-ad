package org.cfcc.utility;

import javaxt.security.ActiveDirectory;

import javax.naming.NamingException;
import javax.naming.ldap.LdapContext;

/**
 * Class to handle the authentication for the web application.
 *
 * Created by Jakim Friant (jfriant80) on 3/23/16.
 */
public class AuthProvider {
    private ActiveDirectory.User current_user;

    public enum SecurityLevels {
        UNAUTHENTICATED, GUEST, AUTHENTICATED, PRIVILEGED
    }

    public AuthProvider() {
        current_user = null;
    }

    public AuthProvider(ActiveDirectory.User user) { current_user = user; }

    /**
     * @param username name of user
     * @param password password of user
     * @param domain_name AD domain name
     * @param server_name AD DC server name
     * @return True if login was success, otherwise false
     */
    public boolean validate(String username, String password, String domain_name, String server_name) {
        if (!username.isEmpty() && !password.isEmpty()) {
            try {
                LdapContext conn = ActiveDirectory.getConnection(username, password, domain_name, server_name);
                current_user = ActiveDirectory.getUser(username, conn);
                conn.close();
                return true;
            } catch (NamingException e) {
                // e.printStackTrace();
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * @param username name of user
     * @param password password of user
     * @param domain_name AD domain name
     * @param server_name AD DC server name
     * @param search_base The OU and domain to search
     * @return True if login was success, otherwise false
     */
    public boolean validate(String username, String password, String domain_name, String server_name, String search_base) {
        if (!username.isEmpty() && !password.isEmpty()) {
            try {
                System.out.println("Connecting to " + server_name + " as " + username);
                LdapContext conn = ActiveDirectory.getConnection(username, password, domain_name, server_name);
                System.out.println("Fetching the current user information");
                current_user = ActiveDirectory.getUser(username, search_base, conn);
                conn.close();
                return true;
            } catch (NamingException e) {
                System.out.println("Authentication failed: " + e.getMessage());
                return false;
            }
        } else {
            System.out.println("Failed because username or password is empty");
            return false;
        }
    }

    public int getSecurityLevel(String group_names) {
        SecurityLevels slvl = SecurityLevels.UNAUTHENTICATED;
        if (current_user != null) {
            // System.out.println("Checking groups for " + current_user.getCommonName());
            slvl = SecurityLevels.AUTHENTICATED;
            memberofloop:
            for (String my_group: current_user.getMemberOf()) {
                for (String auth_group: group_names.split("[|]")) {
                    // System.out.println(auth_group + " == " + my_group);
                    if (my_group.contains(auth_group)) {
                        slvl = SecurityLevels.PRIVILEGED;
                        break memberofloop;
                    }
                }
            }
        } else {
            System.out.println("Current user is NULL");
        }
        return slvl.ordinal();
    }

    public ActiveDirectory.User getCurrent_user() {
        return current_user;
    }
}
