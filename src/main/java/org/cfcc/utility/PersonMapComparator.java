package org.cfcc.utility;

import java.util.Comparator;
import java.util.Map;

public class PersonMapComparator implements Comparator<Map<String, String>> {
    @Override
    public int compare(Map<String, String> o1, Map<String, String> o2) {
        int c = o1.get("person_last").compareTo(o2.get("person_last"));
        if (c != 0) {
            return c;
        } else {
            return o1.get("person_first").compareTo(o2.get("person_first"));
        }
    }
}
