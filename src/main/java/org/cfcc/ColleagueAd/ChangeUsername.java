package org.cfcc.ColleagueAd;
/*
 * ChangeUsername is a web-based tool to rename a user in Colleague.  Now that we've changed away from using DRUS to
 * delete and add the user back, I needed a consistent way to rename the username in PERSON.PIN and ORG.ENTITY.ENV.
 *
 * Created by Jakim Friant (jfriant80) on 3/21/16.
 */
import asjava.uniobjects.UniFileException;
import asjava.uniobjects.UniSessionException;
import org.cfcc.model.ColleagueService;
import org.cfcc.model.OrgEntity;
import org.cfcc.model.OrgEntityEnv;
import org.cfcc.model.PersonPin;
import java.util.Properties;

public class ChangeUsername extends ColleagueService {
    public ChangeUsername(String server_address, String username, String password, String account_path) {
        super(server_address, username, password, account_path);
    }

    public ChangeUsername(Properties config_prop) {
        super(config_prop.getProperty("host"), config_prop.getProperty("username"), config_prop.getProperty("password"), config_prop.getProperty("database"));
    }

    public String get_current_username(String person_id) throws UniSessionException, UniFileException {
        PersonPin person_pin = new PersonPin(uSession);
        person_pin.setId(person_id);
        return person_pin.user_id.get();
    }

    public String get_suggested_username(String person_id) throws UniSessionException, UniFileException {
        StringBuffer suggested_username = new StringBuffer();

        OrgEntity org_entity = new OrgEntity(uSession);
        org_entity.setId(person_id);
        String first_name = org_entity.oe_first_name.get();
        String last_name = org_entity.oe_last_name.get();
        String middle_name = org_entity.oe_middle_name.get();
        suggested_username.append(first_name.substring(0, 1));
        if (!middle_name.isEmpty()) {
            suggested_username.append(middle_name.substring(0, 1));
        }
        suggested_username.append(last_name);
        suggested_username.append(person_id.substring(person_id.length() - 3, person_id.length()));
        return suggested_username.toString().toLowerCase();
    }

    public void rename_user(String person_id, String new_username) throws UniSessionException, UniFileException {
        // change it in PERSON.PIN
        PersonPin person_pin = new PersonPin(uSession);
        person_pin.setId(person_id);
        person_pin.user_id.set(new_username);

        // change it in ORG.ENTITY.ENV
        OrgEntity org_entity = new OrgEntity(uSession);
        OrgEntityEnv org_entity_env = new OrgEntityEnv(uSession);

        org_entity.setId(person_id);
        String oee_id = org_entity.oe_org_entity_env.get();

        org_entity_env.setId(oee_id);
        org_entity_env.oee_username.set(new_username);
    }
}
