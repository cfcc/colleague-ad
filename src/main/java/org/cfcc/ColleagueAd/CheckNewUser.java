package org.cfcc.ColleagueAd;

import asjava.uniobjects.UniFileException;
import asjava.uniobjects.UniSessionException;
import javaxt.security.ActiveDirectory;
import org.cfcc.model.*;

import javax.naming.NamingException;
import javax.naming.ldap.LdapContext;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class CheckNewUser extends ColleagueService {

    private String domain_name;
    private String domain_server;
    private String domain_username;
    private String domain_password;
    private String domain_search_base;
    private String url_prefix;
    private String url_suffix;

    public CheckNewUser(String host, String username, String password, String database) {
        super(host, username, password, database);
    }

    public CheckNewUser(Properties config_prop) {
        super(config_prop.getProperty("host"), config_prop.getProperty("username"), config_prop.getProperty("password"), config_prop.getProperty("database"));
        domain_name = config_prop.getProperty("domain_name", "");
        domain_server = config_prop.getProperty("domain_server", "");
        domain_username = config_prop.getProperty("domain_username", "");
        domain_password = config_prop.getProperty("domain_password", "");
        domain_search_base = config_prop.getProperty("domain_search_base", "");
        url_prefix = config_prop.getProperty("id_card_url_prefix", "");
        url_suffix = config_prop.getProperty("id_card_url_suffix", ".jpg");
    }

    public HashMap<String, String> getColleagueStatus(String person_id) throws UniSessionException {
        HashMap<String, String> result = new HashMap<>();
        String id_card_url = "/images/Icon.blank-person.jpg";
        try {
            Person person = new Person(uSession);
            person.setId(person_id);
            String name = person.last_name.get() + ", " + person.first_name.get() + " " + person.middle_name.get();
            // Also check the privacy flag before displaying the image
            if (person.privacy_flag.get().isEmpty()) {
                if (!url_prefix.isEmpty()) {
                    id_card_url = url_prefix + person_id + url_suffix;
                }
            } else {
                id_card_url = "/images/Icon.blank-person-no-info.jpg";
            }
            result.put("id", person_id);
            result.put("name", name);
            result.put("import_complete", "true");
        } catch (UniFileException ignored) {
            result.put("name", "");
            result.put("import_complete", "false");
        }
        // System.out.println("DEBUG: the ID card URL is: " + id_card_url);
        result.put("id_card_url", id_card_url);
        return result;
    }

    public HashMap<String, String> getApplicantStatus(String person_id) throws UniSessionException {
        /* LIST APPLICANTS 0457443 APPLICANTS.ADDDATE X810.APP.LAST.CU.APPL.STATUS APP.START.TERMS X810.APP.TODAYMINUS2 */

        HashMap<String, String> result = new HashMap<>();

        String last_cu_status = "N/A";
        String start_term = null;
        boolean app_term_valid = false;
        Date start_date = null;

        Applications applications = new Applications(uSession);
        Applicants applicants = new Applicants(uSession);
        applicants.setId(person_id);

        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yy");

        try {
            for (String app_id: applicants.app_applications.get()) {
                applications.setId(app_id);
                ArrayList<String> statuses = new ArrayList<>(applications.appl_status.get());
                ArrayList<String> status_dates = new ArrayList<>(applications.appl_status_date.get());
                ArrayList<String> start_terms = new ArrayList<>(applications.appl_start_term.get());
                try {
                    Date this_date = sdf.parse(status_dates.get(status_dates.size() - 1));
                    if (start_date == null) {
                        start_date = this_date;
                        last_cu_status = statuses.get(statuses.size() - 1);
                        start_term = start_terms.get(start_terms.size() -1);
                    } else {
                        if (this_date.after(start_date)) {
                            start_date = this_date;
                            last_cu_status = statuses.get(statuses.size() - 1);
                            start_term = start_terms.get(start_terms.size() -1);
                        }
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        } catch (UniFileException e) {
            System.err.println("APPLICANTS warning for " + person_id + ": " + e.getMessage());
        }
        // Verify the status matches the query requirements...
        if (last_cu_status.equals("AP") || last_cu_status.equals("MS")) {
            result.put("app_status_complete", "true");
        } else {
            result.put("app_status_complete", "false");
        }
        // ... then update the status code with the description
        if (!last_cu_status.isEmpty() && !last_cu_status.equals("N/A")) {
            StValcodes st_valcodes = new StValcodes(uSession);
            st_valcodes.setId("APPL.STATUS.CONTROLS");
            last_cu_status += " - " + st_valcodes.getDescription(last_cu_status);
        }
        result.put("app_status", last_cu_status);

        // Verify that the start term is current, less than two years old
        if (start_term != null) {
            int start_year = Integer.parseInt(start_term.substring(0, 4));
            int this_year = Calendar.getInstance().get(Calendar.YEAR) - 2;
            if (start_year >= this_year) {
                app_term_valid = true;
            }
        }
        result.put("app_term_valid", String.valueOf(app_term_valid));
        result.put("app_term", start_term);

        SimpleDateFormat sdf_out = new SimpleDateFormat("MMM d, yyyy");
        boolean todayminus2 = false;
        if (start_date != null) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(start_date);
            cal.add(Calendar.DATE, 2);
            cal.getTime();
            Date today;
            today = new Date();
            if (today.after(cal.getTime())) {
                todayminus2 = true;
            }
            result.put("app_date", sdf_out.format(start_date));
        } else {
            result.put("app_date", "N/A");
        }
        result.put("app_date_complete", String.valueOf(todayminus2));

        return result;
    }

    public HashMap<String, String> getStudentStatus(String person_id) {
        HashMap<String, String> result = new HashMap<>();

        Students students = new Students(uSession);
        students.setId(person_id);

        String last_term = "";
        try {
            ArrayList<String> terms = new ArrayList<>(students.stu_terms.get());
            last_term = terms.get(terms.size() - 1);
        } catch (UniFileException | UniSessionException e) {
            System.err.println("STUDENTS warning for " + person_id + ": " + e.getMessage());
        }  catch (IndexOutOfBoundsException e){
            e.printStackTrace();
        }

        boolean term_is_current = false;
        if (!last_term.isEmpty()) {
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
            Date today = new Date();
            Terms terms = new Terms(uSession);
            ArrayList<String> current_terms = terms.select("WITH TERM.END.DATE GE '" + sdf.format(today) + "'");
            for (String this_term : current_terms) {
                if (last_term.equals(this_term)) {
                    term_is_current = true;
                    break;
                }
            }
        }

        String term_status = "";
        String student_type = "";
        // An active CCPP student will remain active regardless of the term's status
        try {
            student_type = students.stu_current_type.get();
        } catch (UniFileException | UniSessionException | NullPointerException e) {
            System.err.println("STUDENTS read error: " + e.getMessage());
        }
        if (student_type.equals("CCPP")) {
            term_status = "CCPP - Active";
        } else {
            if (!last_term.isEmpty()) {
                String acad_level;
                if (last_term.contains("CE")) {
                    acad_level = "CE";
                } else {
                    acad_level = "CU";
                }
                String stu_terms_id = person_id + "*" + last_term + "*" + acad_level;
                StudentTerms stu_terms = new StudentTerms(uSession);
                stu_terms.setId(stu_terms_id);
                try {
                    ArrayList<String> term_list = new ArrayList<>(stu_terms.sttr_status.get());
                    term_status = term_list.get(0);
                } catch (UniFileException | UniSessionException e) {
                    System.err.println("STUDENT.TERMS error: " + e.getMessage());
                } catch (IndexOutOfBoundsException e) {
                    e.printStackTrace();
                }
                if (!term_status.isEmpty()) {
                    StValcodes st_valcodes = new StValcodes(uSession);
                    st_valcodes.setId("STUDENT.TERM.STATUSES");
                    term_status += " - " + st_valcodes.getDescription(term_status);
                }
            }
        }

        result.put("term", last_term);

        result.put("term_is_current", String.valueOf(term_is_current));
        result.put("term_status", term_status);
        if (term_status.startsWith("P")||term_status.startsWith("R")||term_status.startsWith("C")) {
            result.put("term_status_complete", "true");
        } else {
            result.put("term_status_complete", "false");
        }
        return result;
    }

    public HashMap<String, String> getEmployeeStatus(String person_id) {
        HashMap<String, String> result = new HashMap<>();

        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yy");
        SimpleDateFormat sdf_out = new SimpleDateFormat("MMM d, yyyy");
        Date today = new Date();

        String active_position = "";
        String start_date = "";
        boolean is_active = false;
        boolean is_future = false;
        Hrper hrper = new Hrper(uSession);
        hrper.setId(person_id);
        try {
            ArrayList<String> start_dates = new ArrayList<>(hrper.hrp_perpos_start_date.get());
            ArrayList<String> end_dates = new ArrayList<>(hrper.hrp_perpos_end_date.get());
            ArrayList<String> positions = new ArrayList<>(hrper.hrp_perpos_position_id.get());
            for (int i=0; i<start_dates.size(); i++) {
                try {
                    Date this_start_date = sdf.parse(start_dates.get(i));
                    if (this_start_date.before(today)) {
                        if (end_dates.get(i).isEmpty()) {
                            start_date = sdf_out.format(this_start_date);
                            active_position = positions.get(i);
                            is_active = true;
                            break;
                        } else {
                            Date this_end_date = sdf.parse(end_dates.get(i));
                            if (this_end_date.after(today)) {
                                start_date = sdf_out.format(this_start_date);
                                active_position = positions.get(i);
                                is_active = true;
                                break;
                            }
                        }
                    } else {
                        if (end_dates.get(i).isEmpty()) {
                            active_position = positions.get(i);
                            start_date = sdf_out.format(this_start_date);
                            is_future = true;
                            break;
                        }
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        } catch (UniFileException | UniSessionException e) {
            System.err.println("HRPER warning for " + person_id + ": " + e.getMessage());
        }
        // Check to see if HR set the Last Date Worked field in X2HE
        X810IdCard x810_id_card = new X810IdCard(uSession);
        x810_id_card.setId(person_id);
        try {
            String last_day_worked = x810_id_card.x810_id_ldap_end_date.get();
            if (last_day_worked.isEmpty()) {
                result.put("x2he", "END-DATE-OKAY");
                result.put("x2he_msg", "");
            } else {
                result.put("x2he", "END-DATE-WARNING");
                result.put("x2he_msg", last_day_worked);
            }
        } catch (UniFileException | UniSessionException e) {
            System.err.println("X810.ID.CARD warning for " + person_id + ": " + e.getMessage());
        }
        result.put("active_position", active_position);
        result.put("active_position_complete", String.valueOf(is_active));
        result.put("start_date", start_date);
        result.put("start_date_future", String.valueOf(is_future));
        return result;
    }

    public HashMap<String, String> getUsername(String person_id) {
        HashMap<String, String> result = new HashMap<>();
        OrgEntity oe = new OrgEntity(uSession);
        OrgEntityEnv oee = new OrgEntityEnv(uSession);
        boolean success = true;

        oe.setId(person_id);
        String username = "";

        try {
            oee.setId(oe.oe_org_entity_env.get());
            username = oee.oee_username.get();
            result.put("username", username);
        } catch (UniSessionException | UniFileException e) {
            success = false;
            result.put("username", "ERROR");
        }

        try {
            if (!domain_username.isEmpty() && !domain_password.isEmpty()) {
                //System.out.println("[DEBUG] checking active directory...");
                // Set up a connection to Active Directory
                LdapContext conn = ActiveDirectory.getConnection(domain_username, domain_password, domain_name, domain_server);
                if (!username.isEmpty()) {
                    //System.out.println("[DEBUG] ... have username to search ...");
                    ActiveDirectory.User user_rec = ActiveDirectory.getUser(username, domain_search_base, conn);
                    if (user_rec != null) {
                        result.put("activedirectory", "IN-SYNC");
                        //System.out.println("[DEBUG] ... in sync");
                        if (user_rec.isAccountDisabled()) {
                            result.put("aduserstatus", "DISABLED");
                            result.put("aduserstatusmsg", user_rec.getDescription());
                        }
                    } else {
                        result.put("activedirectory", "SYNC-ERROR");
                        result.put("ad_sync_msg", "Exists in Colleague, Missing in Active Directory");
                        //System.out.println("[DEBUG] ... out of sync");
                    }
                } else {
                    // At this point we are trying to make sure no AD record exists
                    //System.out.println("[DEBUG] ... have employeeNumber to search ...");
                    ActiveDirectory.User user_rec = ActiveDirectory.getUserByEmployeeId(person_id, domain_search_base, conn);
                    if (user_rec != null) {
                        result.put("activedirectory", "SYNC-ERROR");
                        result.put("ad_sync_msg", "Missing in Colleague, Exists in Active Directory");
                        //System.out.println("[DEBUG] ... out of sync: " + user_rec);
                        if (user_rec.isAccountDisabled()) {
                            result.put("aduserstatus", "DISABLED");
                            result.put("aduserstatusmsg", user_rec.getDescription());
                        }
                    } else {
                        result.put("activedirectory", "IN-SYNC");
                        //System.out.println("[DEBUG] ... in sync");
                    }
                }
            //} else {
            //    System.out.println("[DEBUG] no domain login information found, not searching AD");
            }
        } catch (NamingException e) {
            System.err.println("[WARNING] naming exception: " + e.getMessage());
            if (!username.isEmpty()) {
                result.put("activedirectory", "SYNC-ERROR");
                result.put("ad_sync_msg", "Exists in Colleague, Missing in Active Directory");
            }
        } catch (NullPointerException e) {
            System.err.println("[WARNING] search exception, user account is missing attributes: " + e.getMessage());
            // e.printStackTrace();
        }
        result.put("username_complete", String.valueOf(success));
        return result;
    }

    public HashMap<String, String> getEPHSchedule() {
        HashMap<String, String> result = new HashMap<>();

        String record_id = "SCHEDULED.PHANTOMS";
        String schedule_name = "EPHWEBLIST";

        ScheduledPhantoms scheduled_phantoms = new ScheduledPhantoms(uSession);
        scheduled_phantoms.setId(record_id);

        SimpleDateFormat sdf_in = new SimpleDateFormat("MM/dd/yy");
        SimpleDateFormat sdf_out = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat sdf_time_in = new SimpleDateFormat("hh:mma");
        SimpleDateFormat sdf_time_out = new SimpleDateFormat("HH:mm");

        ArrayList<ArrayList<String>> my_sched = new ArrayList<>();

        try {
            ArrayList<String> paragraph_id = new ArrayList<>(scheduled_phantoms.scph_paragraph_id.get());
            ArrayList<String> next_run_date = new ArrayList<>(scheduled_phantoms.scph_next_run_date.get());
            ArrayList<String> next_run_time = new ArrayList<>(scheduled_phantoms.scph_next_run_time.get());
            for (int i=0; i<paragraph_id.size(); i++) {
                if (paragraph_id.get(i).startsWith(schedule_name)) {
                    StringBuffer run_date_time = new StringBuffer();
                    try {
                        Date this_run_date = sdf_in.parse(next_run_date.get(i));
                        run_date_time.append(sdf_out.format(this_run_date));
                    } catch (ParseException e) {
                        System.err.println("Error parsing EPH run date: " + e.getMessage());
                    }
                    try {
                        Date this_run_time = sdf_time_in.parse(next_run_time.get(i));
                        run_date_time.append(" ").append(sdf_time_out.format(this_run_time));
                    } catch (ParseException e) {
                        System.err.println("Error parsing EPH run time: " + e.getMessage());
                    }
                    ArrayList<String> schedule = new ArrayList<>(2);
                    schedule.add(run_date_time.toString());
                    schedule.add(next_run_date.get(i) + " " + next_run_time.get(i));
                    // System.out.println("Schedule = " + schedule.get(0) + ", " + schedule.get(1));
                    my_sched.add(schedule);
                }
            }
            if (!my_sched.isEmpty()) {
                my_sched.sort(Comparator.comparing(a -> a.get(0)));
                result.put("next_run_time", my_sched.get(0).get(1));
            } else {
                result.put("next_run_time", "unknown");
            }
        } catch (UniFileException | UniSessionException e) {
            System.err.println("Error reading  '" + record_id + "' from SCHEDULED.PHANTOMS: " + e.getMessage());
        }
        return result;
    }
}
