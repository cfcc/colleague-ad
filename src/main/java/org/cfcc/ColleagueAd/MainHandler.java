package org.cfcc.ColleagueAd;

import org.cfcc.findperson.FindPersonController;
import org.cfcc.grouprule.GroupRuleController;
import org.cfcc.login.LoginController;

import asjava.uniobjects.UniFileException;
import asjava.uniobjects.UniSessionException;
import asjava.unirpc.UniRPCConnectionException;
import com.google.gson.Gson;
import org.cfcc.ownerset.OwnerSetController;
import org.cfcc.utility.ConfigProp;
import org.cfcc.utility.StandardResponse;
import org.cfcc.utility.StatusResponse;
import spark.ModelAndView;
import spark.template.freemarker.FreeMarkerEngine;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import static spark.Spark.*;
import static spark.Spark.exception;

public class MainHandler {
    public static void main(String[] args) {

        Properties config_prop = ConfigProp.load();

        // Secure the app with a certificate if available
        if (config_prop.containsKey("keystore_filename") && !config_prop.getProperty("keystore_filename").isEmpty()) {
            secure(config_prop.getProperty("keystore_filename"), config_prop.getProperty("keystore_password"), null, null);
            // We also can set the SSL port to something other than the default 4567, though 443 requires running as root
            if (config_prop.containsKey("ssl_port")) {
                port(Integer.parseInt(config_prop.getProperty("ssl_port")));
            }
        }

        staticFileLocation("/public"); // Static files

        get("/", (request, response) -> {
            Map<String, Object> view_objects = new HashMap<>();
            view_objects.put("templateName", "index.ftl");
            view_objects.put("page_title", "Colleague AD Tools");
            view_objects.put("page_description", "These are a set of tools that interact with Colleague and Active Directory.");
            view_objects.putAll(LoginController.check_login_no_redirect(request, response, true));
            return new ModelAndView(view_objects, "layout.ftl");
        }, new FreeMarkerEngine());

        get("/login", LoginController.serveLoginPage, new FreeMarkerEngine());

        post("/login", LoginController.handleLoginPost);

        get("/logout", LoginController.handleLogoutPost);

        get("/user", (request, response) -> {
            Map<String, Object> attributes = new HashMap<>();
            attributes.put("templateName", "get_user.ftl");
            attributes.putAll(LoginController.check_login(request, response));
            return new ModelAndView(attributes, "layout.ftl");
        }, new FreeMarkerEngine());

        post("/user/get", (request, response) -> {
            String person_id = request.queryParams("person_id").trim();
            Map<String, Object> attributes = new HashMap<>();
            attributes.put("templateName", "get_new_name.ftl");
            attributes.putAll(LoginController.check_login(request, response));
            attributes.put("person_id", person_id);
            ChangeUsername instance = new ChangeUsername(config_prop);
            instance.connect();
            try {
                attributes.put("current_username", instance.get_current_username(person_id));
                try {
                    attributes.put("suggested_username", instance.get_suggested_username(person_id));
                } catch (UniFileException ex) {
                    StringBuffer msg = new StringBuffer();
                    String curr_msg = (String) attributes.get("message");
                    if (curr_msg != null) {
                        msg.append(curr_msg).append("\n");
                    }
                    msg.append("Error loading suggested username for ").append(person_id).append(": ").append(ex.toString());
                    attributes.put("message", msg);
                    attributes.put("message_category", "danger");
                    attributes.put("suggested_username", "");
                }
            } catch (UniFileException ex) {
                // If we can't get the original username, the person ID is probably incorrect, go back to the start
                attributes.put("templateName", "get_user.ftl");
                StringBuffer msg = new StringBuffer();
                String curr_msg = (String) attributes.get("message");
                if (curr_msg != null) {
                    msg.append(curr_msg).append("\n");
                }
                msg.append("Error loading current username for ").append(person_id).append(": ").append(ex.toString());
                attributes.put("message", msg.toString());
                attributes.put("message_category", "danger");
                attributes.put("current_username", "");
            }
            return new ModelAndView(attributes, "layout.ftl");
        }, new FreeMarkerEngine());

        post("/user/rename", (request, response) -> {
            String person_id = request.queryParams("person_id");
            String current_username = request.queryParams("current_username");
            String new_username = request.queryParams("new_username");
            Map<String, Object> attributes = new HashMap<>();
            attributes.put("templateName", "get_user.ftl");
            ChangeUsername instance = new ChangeUsername(config_prop);
            instance.connect();
            attributes.putAll(LoginController.check_login(request, response));
            try {
                instance.rename_user(person_id.trim(), new_username.trim());
                attributes.put("message", "Renamed user (" + person_id + "): " + current_username + " => " + new_username);
            } catch (Exception e) {
                attributes.put("message_category", "danger");
                attributes.put("message", "ERROR: Renaming user (" + person_id + ") from " + current_username + " to " + new_username + " failed: " + e.getLocalizedMessage());
            }
            return new ModelAndView(attributes, "layout.ftl");
        }, new FreeMarkerEngine());

        get("/status", (request, response)->{
            Map<String, Object> attributes = new HashMap<>();
            attributes.put("templateName", "status.ftl");
            attributes.putAll(LoginController.check_login(request, response));
            return new ModelAndView(attributes, "layout.ftl");
        }, new FreeMarkerEngine());

        get("/check/:id", (req,res)->{
            res.type("application/json");
            Map<String, Object> attributes = LoginController.check_login(req, res);
            if ((int) attributes.getOrDefault("securityLevel", 0) > 0) {
                // Set up a connection to Colleague
                CheckNewUser instance = new CheckNewUser(config_prop);
                instance.connect();
                // Clean up the person_id
                StringBuffer person_id = new StringBuffer(req.params(":id").trim());
                if (person_id.length() < 7) {
                    int leading_zeros = 7 - person_id.length();
                    for (int i=0; i<leading_zeros; i++) {
                        person_id.insert(0, "0");
                    }
                }
                HashMap<String, HashMap<String, String>> result = new HashMap<String, HashMap<String, String>>();
                result.put("colleague", instance.getColleagueStatus(person_id.toString()));
                result.put("applicant", instance.getApplicantStatus(person_id.toString()));
                result.put("student", instance.getStudentStatus(person_id.toString()));
                result.put("employee", instance.getEmployeeStatus(person_id.toString()));
                result.put("user", instance.getUsername(person_id.toString()));
                result.put("eph", instance.getEPHSchedule());
                return new Gson().toJson(new StandardResponse(StatusResponse.SUCCESS, new Gson().toJsonTree(result)));
            } else {
                String msg = "User " +
                        attributes.getOrDefault("username", "UNKNOWN") +
                        " (securityLevel: " +
                        attributes.getOrDefault("securityLevel", "?") +
                        ") is not authorized";
                System.err.println(msg);
                return new Gson().toJson(new StandardResponse(StatusResponse.ERROR, "Not Authorized"));
            }
        });

        get("/group", GroupRuleController.fetchAllGroupRules, new FreeMarkerEngine());

        get("/group/:id", GroupRuleController.fetchOneGroupRule);

        post("/group/:id", GroupRuleController.saveGroupRule);

        delete("/group/:id", GroupRuleController.deleteGroupRule);

        get("/ownerset", OwnerSetController.fetchAllOwnerSets, new FreeMarkerEngine());

        post("/ownerset/create", OwnerSetController.createOwnerSet);

        get("/ownerset/:id", OwnerSetController.fetchOneOwnerSet);

        post("/ownerset/:id", OwnerSetController.saveOwnerSet);

        delete("/ownerset/:id", OwnerSetController.deleteOwnerSet);

        delete("/owner/:id", OwnerSetController.deleteOwner);

        get("/person/search", FindPersonController.findByName);

        get("/person/:id", FindPersonController.fetchOnePerson);

        exception(UniSessionException.class, (e, request, response) -> {
            response.status(404);
            response.body("Database Session Failed: " + e.toString());
        });
        exception(UniRPCConnectionException.class, (e, request, response) -> {
            response.status(404);
            response.body("Database RPC Connection Failed: " + e.toString());
        });
        exception(UniFileException.class, (e, request, response) -> {
            response.status(404);
            response.body("Database File Connection Failed: " + e.toString());
        });
    }
}
