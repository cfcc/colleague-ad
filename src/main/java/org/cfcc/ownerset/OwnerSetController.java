package org.cfcc.ownerset;

import com.google.gson.Gson;
import org.cfcc.findperson.FindPersonModel;
import org.cfcc.login.LoginController;
import org.cfcc.utility.ConfigProp;
import org.cfcc.utility.PersonMapComparator;
import org.cfcc.utility.StandardResponse;
import org.cfcc.utility.StatusResponse;
import spark.*;

import java.util.*;

public class OwnerSetController {
    public static TemplateViewRoute fetchAllOwnerSets = (Request request, Response response) -> {
        Map<String, Object> attributes = new HashMap<>();

        Properties config_prop = ConfigProp.load();

        OwnerSetModel instance = new OwnerSetModel(config_prop);
        attributes.put("owner_sets", instance.get_all_owner_sets());

        attributes.put("templateName", "owner_set_list.ftl");
        attributes.putAll(LoginController.check_login(request, response));

        return new ModelAndView(attributes, "layout.ftl");
    };

    public static Route fetchOneOwnerSet = (Request req, Response res)->{
        res.type("application/json");
        Map<String, Object> attributes = LoginController.check_login(req, res);
        if ((int) attributes.getOrDefault("securityLevel", 0) >= 0) {
            HashMap<String, Object> result = new HashMap<>();
            int group_id = Integer.parseInt(req.params(":id"));
            Properties config_prop = ConfigProp.load();
            OwnerSetModel instance = new OwnerSetModel(config_prop);
            result.put("owners_group_name", instance.get_owners_group_name(group_id));
            ArrayList<HashMap<String, String>> owner_ids = instance.get_owners(group_id);

            FindPersonModel instance2 = new FindPersonModel(config_prop);
            instance2.connect();
            ArrayList<HashMap<String, String>> owner_names = new ArrayList<HashMap<String, String>>();
            for (HashMap<String,String> row : owner_ids) {
                String[] name = instance2.get_person_name(row.get("person_id"));
                row.put("person_first", name[1]);
                row.put("person_last", name[0]);
                owner_names.add(row);
            }
            Collections.sort(owner_names, new PersonMapComparator());
            result.put("owners", owner_names);

            return new Gson().toJson(new StandardResponse(StatusResponse.SUCCESS, new Gson().toJsonTree(result)));
        } else {
            String msg = "User " +
                    attributes.getOrDefault("username", "UNKNOWN") +
                    " (securityLevel: " +
                    attributes.getOrDefault("securityLevel", "?") +
                    ") is not authorized";
            System.err.println(msg);
            res.status(401);
            return new Gson().toJson(new StandardResponse(StatusResponse.ERROR, "Not Authorized"));
        }
    };

    public static Route saveOwnerSet = (Request request, Response response) -> {
        response.type("application/json");
        Map<String, Object> attributes = LoginController.check_login(request, response);
        if ((int) attributes.getOrDefault("securityLevel", 0) >= 3) {
            HashMap<String, String> form_data = new HashMap<>();
            for(String key: request.queryParams()) {
                //System.out.println("[DEBUG] saveGroupRule() " + key + ": " + request.queryParams(key));
                form_data.put(key, request.queryParams(key));
            }
            form_data.put("id", request.params(":id"));
            Properties config_prop = ConfigProp.load();
            OwnerSetModel instance = new OwnerSetModel(config_prop);
            if (instance.add_owner_to_set(form_data)) {
                return new Gson().toJson(new StandardResponse(StatusResponse.SUCCESS, "Record updated"));
            } else {
                response.status(500);
                return new Gson().toJson(new StandardResponse(StatusResponse.ERROR, "Error saving record"));
            }
        } else {
            String msg = "User " +
                    attributes.getOrDefault("username", "UNKNOWN") +
                    " (securityLevel: " +
                    attributes.getOrDefault("securityLevel", "?") +
                    ") is not authorized";
            System.err.println(msg);
            response.status(401);
            return new Gson().toJson(new StandardResponse(StatusResponse.ERROR, "Not Authorized"));
        }
    };

    public static Route createOwnerSet = (Request request, Response response) -> {
        response.type("application/json");
        Map<String, Object> attributes = LoginController.check_login(request, response);
        if ((int) attributes.getOrDefault("securityLevel", 0) >= 3) {
            //System.out.println("[DEBUG] createOwnerSet() " + request.queryParams());
            String new_name = request.queryParams("new_name");
            Properties config_prop = ConfigProp.load();
            OwnerSetModel instance = new OwnerSetModel(config_prop);
            if (instance.create_owner_set(new_name)) {
                return new Gson().toJson(new StandardResponse(StatusResponse.SUCCESS, "Record created"));
            } else {
                response.status(500);
                return new Gson().toJson(new StandardResponse(StatusResponse.ERROR, "Error creating record"));     
            }
        } else {
            String msg = "User " +
                    attributes.getOrDefault("username", "UNKNOWN") +
                    " (securityLevel: " +
                    attributes.getOrDefault("securityLevel", "?") +
                    ") is not authorized";
            System.err.println(msg);
            response.status(401);
            return new Gson().toJson(new StandardResponse(StatusResponse.ERROR, "Not Authorized"));
        }
    };

    public static Route deleteOwner = (Request request, Response response) -> {
        response.type("application/json");
        Map<String, Object> attributes = LoginController.check_login(request, response);
        if ((int) attributes.getOrDefault("securityLevel", 0) >= 3) {
            int owner_id = Integer.parseInt(request.params(":id"));
            Properties config_prop = ConfigProp.load();
            OwnerSetModel instance = new OwnerSetModel(config_prop);
            if (instance.delete_owner_from_set(owner_id)) {
                return new Gson().toJson(new StandardResponse(StatusResponse.SUCCESS, "Record deleted"));
            } else {
                response.status(500);
                return new Gson().toJson(new StandardResponse(StatusResponse.ERROR, "Error deleting record"));
            }
        } else {
            String msg = "User " +
                    attributes.getOrDefault("username", "UNKNOWN") +
                    " (securityLevel: " +
                    attributes.getOrDefault("securityLevel", "?") +
                    ") is not authorized";
            System.err.println(msg);
            response.status(401);
            return new Gson().toJson(new StandardResponse(StatusResponse.ERROR, "Not Authorized"));
        }
    };

    public static Route deleteOwnerSet = (Request request, Response response) -> {
        response.type("application/json");
        Map<String, Object> attributes = LoginController.check_login(request, response);
        if ((int) attributes.getOrDefault("securityLevel", 0) >= 3) {
            int owner_set_id = Integer.parseInt(request.params(":id"));
            Properties config_prop = ConfigProp.load();
            OwnerSetModel instance = new OwnerSetModel(config_prop);
            if (instance.delete_owner_set(owner_set_id)) {
                return new Gson().toJson(new StandardResponse(StatusResponse.SUCCESS, "Deleted " + owner_set_id));
            } else {
                response.status(500);
                return new Gson().toJson(new StandardResponse(StatusResponse.ERROR, "Error deleting " + owner_set_id));
            }
        } else {
            String msg = "User " +
                    attributes.getOrDefault("username", "UNKNOWN") +
                    " (securityLevel: " +
                    attributes.getOrDefault("securityLevel", "?") +
                    ") is not authorized";
            System.err.println(msg);
            response.status(401);
            return new Gson().toJson(new StandardResponse(StatusResponse.ERROR, "Not Authorized"));
        }
    };
}
