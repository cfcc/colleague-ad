package org.cfcc.ownerset;

import org.cfcc.model.SQLService;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;

public class OwnerSetModel extends SQLService {

    public OwnerSetModel(String username, String password, String hostname, String db_name) {
        super(username, password, hostname, db_name);
    }

    public OwnerSetModel(Properties config_prop) {
        super(config_prop.getProperty("sql_username"),
                config_prop.getProperty("sql_password"),
                config_prop.getProperty("sql_hostname"),
                config_prop.getProperty("sql_db_name"));
    }

    public ArrayList<HashMap<String, String>> get_all_owner_sets() {
        ArrayList<HashMap<String, String>> result = new ArrayList<>();
        String query1 = "SELECT id, name FROM owner_set ORDER BY name";
        String query2 = "SELECT class_name, group_name FROM rules WHERE owner_set_id = ?";
        try {
            Connection con = ds.getConnection();
            CallableStatement cstmt = con.prepareCall(query1);
            ResultSet rs = cstmt.executeQuery();
            while (rs.next()) {
                HashMap<String, String> row = new HashMap<>();
                String owner_set_id = rs.getString("id");
                row.put("id", owner_set_id);
                row.put("name", rs.getString("name"));
                StringBuffer groups_using_set = new StringBuffer();
                try {
                    Connection con2 = ds.getConnection();
                    CallableStatement cstmt2 = con2.prepareCall(query2);
                    cstmt2.setString(1, owner_set_id);
                    ResultSet rs2 = cstmt2.executeQuery();
                    boolean first = true;
                    while (rs2.next()) {
                        // String class_name = rs2.getString("class_name");
                        String group_name = rs2.getString("group_name");
                        if (first) {
                            first = false;
                        } else {
                            groups_using_set.append(", ");
                        }
                        // groups_using_set.append(class_name).append(" (").append(group_name).append(")");
                        groups_using_set.append(group_name);
                    }
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                }
                row.put("groups_using_set", groups_using_set.toString());
                result.add(row);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return result;
    }

    public ArrayList<HashMap<String, String>> get_owners(int rule_id) {
        String query = "SELECT id, person_id FROM owners WHERE owner_set_id = ?";
        ArrayList<HashMap<String, String>> result = new ArrayList<HashMap<String, String>>();
        try {
            Connection con = ds.getConnection();
            CallableStatement cstmt = con.prepareCall(query);
            cstmt.setInt(1, rule_id);
            ResultSet rs = cstmt.executeQuery();
            while (rs.next()) {
                HashMap<String, String> row = new HashMap<>();
                row.put("id", rs.getString("id"));
                row.put("person_id", rs.getString("person_id"));
                result.add(row);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return result;
    }

    public String get_owners_group_name(int group_id) {
        String query = "SELECT name FROM owner_set WHERE id = ?";
        String result = null;
        try {
            Connection con = ds.getConnection();
            CallableStatement cstmt = con.prepareCall(query);
            cstmt.setInt(1, group_id);
            ResultSet rs = cstmt.executeQuery();
            if (rs.next()) {
                result = rs.getString("name");
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return result;
    }

    public boolean add_owner_to_set(HashMap<String, String> form_data) {
        boolean success = true;
        int owner_set_id = Integer.parseInt(form_data.get("id"));
        for (String key: form_data.keySet()) {
            if (key.startsWith("owner_id")) {
                String person_id = form_data.get(key);
                // search the database for they person_id and owner_set_id
                // if it doesn't exist, create the record
                String query = "SELECT id FROM owners WHERE owner_set_id = ? AND person_id = ?";
                String query2 = "INSERT INTO owners (owner_set_id, person_id) VALUES (?, ?)";
                try {
                    Connection con = ds.getConnection();
                    CallableStatement cstmt = con.prepareCall(query);
                    cstmt.setInt(1, owner_set_id);
                    cstmt.setString(2, person_id);
                    ResultSet rs = cstmt.executeQuery();
                    if (! rs.next()) {
                        PreparedStatement ps2 = con.prepareStatement(query2);
                        ps2.setInt(1, owner_set_id);
                        ps2.setString(2, person_id);
                        int affected_rows = ps2.executeUpdate();
                        if (affected_rows == 0) {
                            throw new SQLException("Failed to update record");
                        }
                        //System.out.println("[DEBUG] addded " + person_id + " to ownerset " + owner_set_id);
                        success = true;
                    //} else {
                    //    System.out.println("[DEBUG] Duplicate owner ID ignored: " + person_id);
                    }
                } catch (SQLException e) {
                    success = false;
                    e.printStackTrace();
                }
            }
        }
        //System.out.println("[DEBUG] owner_set_id = " + owner_set_id);
        //System.out.println("[DEBUG] " + form_data);
        return success;
    }

    public boolean delete_owner_from_set(int owner_id) {
        boolean success = false;
        String query = "DELETE FROM owners WHERE id = ?";
        try {
            Connection con = ds.getConnection();
            PreparedStatement ps = con.prepareStatement(query);
            ps.setInt(1, owner_id);
            int rows_updated = ps.executeUpdate();
            if (rows_updated >= 1) {
                //System.out.println("[DEBUG] deleted owner id " + owner_id);
                success = true;
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return success;
    }

    public boolean create_owner_set(String new_name) {
        boolean success = false;
        String query = "INSERT INTO owner_set (name) VALUES (?)";
        try {
            Connection con = ds.getConnection();
            PreparedStatement ps = con.prepareStatement(query);
            ps.setString(1, new_name);
            int rows_updated = ps.executeUpdate();
            if (rows_updated >= 1) {
                //System.out.println("[DEBUG] created new owner set " + new_name);
                success = true;
            }
        } catch (SQLException exc) {
            exc.printStackTrace();
        }
        return success;
    }

    public boolean delete_owner_set(int owner_set_id) {
        boolean success = false;
        String query1 = "UPDATE rules SET owner_set_id = null WHERE owner_set_id = ?";
        String query2 = "DELETE FROM owners WHERE owner_set_id = ?";
        String query3 = "DELETE FROM owner_set WHERE id = ?";
        try {
            Connection con = ds.getConnection();

            PreparedStatement ps3 = con.prepareStatement(query1);
            ps3.setInt(1, owner_set_id);
            ps3.executeUpdate();

            PreparedStatement ps1 = con.prepareStatement(query2);
            ps1.setInt(1, owner_set_id);
            ps1.executeUpdate();

            PreparedStatement ps2 = con.prepareStatement(query3);
            ps2.setInt(1, owner_set_id);
            int rows_updated = ps2.executeUpdate();
            if (rows_updated > 0) {
                success = true;
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return success;
    }
}
