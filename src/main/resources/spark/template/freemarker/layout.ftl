<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <#if page_stylesheet??><link rel="stylesheet" href="${page_stylesheet}"></#if>
    <#if page_javascript??><script src="${page_javascript}"></script></#if>

    <!-- bootstrap -->
    <link rel="stylesheet" href="/bootstrap-4.6.2/css/bootstrap.css">
    <link rel="stylesheet" href="/css/jumbotron.css">
    <link rel="stylesheet" href="/css/all.css">

    <!-- jquery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="/js/jquery-3.5.1.min.js"></script>
    <script src="/bootstrap-4.6.2/js/bootstrap.bundle.js"></script>

    <title>Colleague AD</title>
</head>
<body>

    <nav class="navbar navbar-expand-lg navbar-dark fixed-top bg-dark">
        <a class="navbar-brand" href="/"><i class="fas fa-home"></i></a>
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="/">Home</a>
            </li>
        </ul>
    <#if logged_in??>
        <form class="form-inline" action="/logout">
            <button type="submit" class="btn btn-outline-success my-2 my-sm-0">Sign Out <i class="fas fa-sign-out-alt"></i></button>
        </form>
    <#else>
        <form class="form-inline" action="/login" method="post">
            <input type="text" name="login_username" placeholder="Username" class="form-control mr-sm-2" autofocus>
            <input type="password" name="login_password" placeholder="Password" class="form-control mr-sm-2">
            <button type="submit" class="btn btn-outline-success my-2 my-sm-0">Sign In <i class="fas fa-sign-in-alt"></i></button>
        </form>
    </#if>
    </nav>

    <#if page_title??>
    <div class="jumbotron">
        <div class="container">
            <h3>${page_title}</h3>
            <#if page_description??><p>${page_description}</p></#if>
        </div>
    </div>
    </#if>

    <div class="container">
        <div id="messagesArea">
            <#if message??>
                <div class="alert alert-${message_category!'info'} flash mt-2" role="alert">
                    ${message}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </#if>
        </div>
        <#include "${templateName}">
    </div><!-- /.container -->
</body>
</html>