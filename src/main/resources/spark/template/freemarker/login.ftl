<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Colleague AD</title>

    <!-- bootstrap -->
    <link rel="stylesheet" href="/bootstrap-4.6.2/css/bootstrap.css">
    <link rel="stylesheet" href="/css/jumbotron.css">
    <link rel="stylesheet" href="/css/all.css">
</head>
<body>

<nav class="navbar navbar-expand-lg navbar-dark fixed-top bg-dark">
    <a class="navbar-brand" href="/"><i class="fas fa-home"></i></a>
    <ul class="navbar-nav mr-auto">
        <li class="nav-item">
            <a class="nav-link" href="/">Home</a>
        </li>
    </ul>
</nav>
<div class="container">
    <div class="row">
        <div class="col-6 mt-4">
            <h2>Login</h2>
            <#if error??>
                <div class="alert alert-danger flash" role="alert">
                    <p><strong>Error:</strong> ${error}</p>
                </div>
            </#if>

            <form action="/login" method="post">
                <div class="form-group">
                    <label for="login_username">Username:</label>
                    <input type="text" class="form-control" name="login_username" id="login_username" autofocus>
                </div>
                <div class="form-group">
                    <label for="login_password">Password:</label>
                    <input type="password" class="form-control" name="login_password" id="login_password">
                </div>
                <button type="submit" class="btn btn-primary">Sign In <i class="fas fa-sign-in-alt"></i></button>
            </form>
        </div>
    </div>
</div>
<!-- jquery (necessary for Bootstrap's JavaScript plugins) -->
<script src="/js/jquery-3.5.1.min.js"></script>
<script src="/bootstrap-4.6.2/js/bootstrap.js"></script>
</body>
</html>