<div class="row mt-4">
    <h4>Rename User</h4>
</div>
<#if logged_in??>
    <#if securityLevel gte 3>
        <form action="/user/get" method="post" class="add-entry">
            <label for="f_person_id">Person ID Number:</label>
            <input type="text" size="30" name="person_id" id="f_person_id" autofocus/>
            <input type="submit" value="Submit" class="btn btn-lg btn-primary"/>
        </form>
    <#else>
        <div class="alert alert-warning flash" role="alert">
            <h2>Warning</h2>
            <p>You are not authorized to access this page.</p>
        </div>
    </#if>
<#else>
    <div class="alert alert-danger flash" role="alert">
        <h4>Error</h4>
        <p>You must be logged in to access this page.</p>
    </div>
</#if>
