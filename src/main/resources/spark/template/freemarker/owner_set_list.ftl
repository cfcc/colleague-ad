<div class="row my-4 justify-content-between">
    <div class="col">
        <h4>AD Rule Owner Set</h4>
    </div>
    <div class="col">
        <a href="/group" class="btn btn-primary float-right">Group Builder Rules</a>
    </div>
</div>
<#if logged_in??>
    <p><button type="button" class="btn btn-success" onclick="show_new_owner_dialog();">Add Owner Set</button></p>
    <table class="table">
        <thead>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Associated Group Rules</th>
            <th></th>
        </tr>
        </thead>
        <#list owner_sets as this_row>
            <tr id="owner_row_${this_row.id}">
                <td>
                    ${this_row.id}
                </td>
                <td>
                    <a href="#" onclick="do_edit_owners(${this_row.id});" class="btn btn-light">
                        ${this_row.name}
                        <i class="fas fa-edit"></i>
                    </a>
                </td>
                <td>${this_row.groups_using_set}</td>
                <td>
                    <button type="button" onclick="show_delete_owner_dialog(${this_row.id});" class="btn btn-danger btn-sm">
                        <i class="fas fa-trash"></i>
                    </button>
                </td>
            </tr>
        </#list>
    </table>
    <!-- New Owner Set dialog box -->
    <div class="modal" tabindex="-1" role="dialog" id="newOwnerSetDialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Add Owner Set</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="createOwnerSetForm">
                        <div class="form-group">
                            <label for="new_owner_set_name">Name</label>
                            <input type="text" class="form-control" id="new_owner_set_name" name="new_owner_set_name" />
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" onclick="do_add_new_owner_set();">Save changes</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Owners Group editor dialog box -->
    <div class="modal" tabindex="-1" role="dialog" id="editOwners">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Edit Owners list: <span id="owners_name_placeholder"></span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="owners_list_form">
                        <div id="owners_list"></div>
                        <div class="row">
                            <div class="col">
                                <input type="text" class="form-control" name="new_owner_id" id="new_owner_id" />
                            </div>
                            <div class="col">
                                <button type="button" class="btn btn-primary" onclick="add_new_owner();">Add Owner</button>
                            </div>
                            <div class="col"></div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" onclick="do_save();">Save changes</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Delete Owner Set dialog box -->
    <div class="modal" tabindex="-1" role="dialog" id="deleteOwnerSet">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Delete Owner Set</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="delete_owner_set_form">
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" name="can_delete" id="can_delete" />
                            <label for="can_delete" class="form-check-label"><span id="delete_owner_set_message"></span></label>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary" onclick="do_delete_owner_set();">Confirm</button>
                </div>
            </div>
        </div>
    </div>
    <script>
        function do_edit_owners(record_id) {
            $('#owners_list').empty();
            $('#new_owner_id').val("").attr("sequence", 0);
            $('#owners_list_form').attr('record_id', record_id);
            $.ajax('/ownerset/' + record_id)
                .done( function( resp ) {
                    $.each(resp['data']['owners'], function (i, item) {
                        let person_name = item.person_last + ", " + item.person_first;
                        add_row(0, item.id, item.person_id, person_name);
                    })
                    $('#owners_name_placeholder').text(resp['data']['owners_group_name']);
                })
                .always( function( resp ) {
                    $('#editOwners').modal();
                })
        }
        function add_row(seq_id, owner_id, per_id, per_name, read_only) {
            let row = document.createElement('div');
            row.setAttribute('class', 'form-row my-1');
            row.setAttribute('data-owner-id', owner_id);
            let col1 = document.createElement('div');
            col1.setAttribute('class', 'col');
            if (read_only) {
                let j = document.createElement('input');
                j.setAttribute('type', 'text');
                j.setAttribute('name', 'owner_id[' + seq_id + ']');
                j.setAttribute('class', 'form-control');
                j.setAttribute('value', per_id);
                col1.appendChild(j);
            } else {
                let j = document.createElement('p');
                j.innerText = per_id;
                col1.appendChild(j);
            }
            let col2 = document.createElement('div');
            col2.setAttribute('class', 'col');
            let i = document.createElement('p');
            i.innerText = per_name;
            col2.appendChild(i);
            let col3 = document.createElement('div');
            col3.setAttribute('class', 'col');
            let k = document.createElement('button');
            k.setAttribute('type', 'button');
            k.setAttribute('class', 'btn btn-danger btn-sm float-right');
            // k.innerText = 'Del';
            let l = document.createElement('i');
            l.setAttribute('class', 'fas fa-trash');
            l.onclick = function(){ do_delete(owner_id); };
            k.appendChild(l);
            col3.appendChild(k);
            row.appendChild(col1);
            row.appendChild(col2);
            row.appendChild(col3);
            $('#owners_list').append(row);
        }
        function add_new_owner() {
            let seq_id = parseInt($('#new_owner_id').attr('sequence')) - 1;
            let owner_id = seq_id;
            let person_id = $('#new_owner_id').val();
            //console.log('[DEBUG] add_new_owner() ' + person_id);
            if (person_id) {
                $.ajax('/person/' + person_id)
                    .done(function (resp) {
                        let person_name = resp['data']['person_last'] + ", " + resp['data']['person_first']
                        add_row(seq_id, owner_id, person_id, person_name, true);
                        $('#new_owner_id').val("").attr('sequence', seq_id);
                    })
            }
        }
        function do_save() {
            let record_id = $('#owners_list_form').attr('record_id');
            let form_values = $('#owners_list_form').serialize();
            $.ajax({
                type: 'POST',
                url: '/ownerset/' + record_id,
                data: form_values
            })
                .fail(function(jqXHR, textStatus, error) {
                    let error_json = $.parseJSON(jqXHR.responseText)
                    let error_msg = error_json["message"];
                    $('#messagesArea').html('<div class="alert alert-danger flash mt-2" role="alert">Error saving record: ' + error_msg + '</div>');
                })
                .always(function () {
                    $('#editOwners').modal('hide');
                })
        }
        function do_delete(owner_id) {
            if (owner_id > 0) {
                $.ajax({
                    type: 'DELETE',
                    url: '/owner/' + owner_id
                })
                .done(function() {
                    $('div [data-owner-id=' + owner_id + ']').empty();
                })
            } else {
                $('div [data-owner-id="' + owner_id + '"]').empty();
            }
        }
        function show_new_owner_dialog() {
            $('#new_owner_set_name').val("");
            $('#newOwnerSetDialog').modal();
        }
        function do_add_new_owner_set() {
            let new_name = $('#new_owner_set_name').val();
            //console.log('[DEBUG] ' + new_name);
            $.ajax({
                type: 'POST',
                url: '/ownerset/create',
                data: { 'new_name': new_name }
            })
            .done(function() {
                $('#newOwnerSetDialog').modal('hide');
                location.reload();
            });
        }
        function show_delete_owner_dialog(owner_set_id) {
            $('#can_delete').prop('checked', false);
            $('#delete_owner_set_message').text("Delete owner set #" + owner_set_id).attr('data-owner-set-id', owner_set_id);
            $('#deleteOwnerSet').modal('show');
        }
        function do_delete_owner_set() {
            let owner_set_id = $('#delete_owner_set_message').attr('data-owner-set-id');
            let row_name = "#owner_row_" + owner_set_id;
            if ($('#can_delete').prop('checked')) {
                $('#deleteOwnerSet').modal('hide');
                // console.log("[DEBUG] removing " + row_name);
                $.ajax({
                    type: 'DELETE',
                    url: '/ownerset/' + owner_set_id,
                })
                    .done(function () {
                        $(row_name).empty();
                    })
                    .fail(function (jqXHR, textStatus, error) {
                        let error_json = $.parseJSON(jqXHR.responseText)
                        let error_msg = error_json["message"];
                        $('#messagesArea').html('<div class="alert alert-danger flash mt-2" role="alert">Error deleting record: ' + error_msg + '</div>');
                    });
            }
        }
    </script>
<#else>
    <div class="alert alert-danger flash" role="alert">
        <h4>Error</h4>
        <p>You must be logged in to access this page.</p>
    </div>
</#if>
