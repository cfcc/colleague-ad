<div class="row my-4 justify-content-between">
    <div class="col">
        <h4>AD Group Builder Rules</h4>
    </div>
    <div class="col">
        <a href="/ownerset" class="btn btn-primary float-right">Owner Sets</a>
    </div>
</div>
<#if logged_in??>
    <p><button type="button" onclick="do_edit_rule(-1);" class="btn btn-success">Add Group Rule</button></p>
    <table class="table" id="grouplist">
        <thead>
            <tr>
                <th>ID</th>
                <th>Java Plugin Name</th>
                <th>AD Group Name</th>
                <th>Status</th>
                <th>Description</th>
                <th></th>
            </tr>
        </thead>
        <#list group_list as row>
            <tr id="group_rule_row_${row.id}">
                <td>${row.id}</td>
                <td>
                    <button type="button" onclick="do_edit_rule(${row.id});" class="btn btn-light">${row.class_name!"NULL"} <i class="fas fa-edit"></i></button>
                </td>
                <td>${row.group_name!""}</td>
                <td>
                    <#if (row.disabled == "1")>
                        <span class="badge badge-danger">Disabled</span><br>
                    </#if>
                    <#if (row.run_alone == "1")>
                        <span class="badge badge-warning">Run Alone</span><br>
                    </#if>
                    <#if (row.after_hours == "1")>
                        <span class="badge badge-info">After Hours</span>
                    </#if>
                </td>
                <td>${row.group_desc!""}</td>
                <td>
                    <button type="button" onclick="show_delete_group_rule_dialog(${row.id});" class="btn btn-danger btn-sm">
                        <i class="fas fa-trash"></i>
                    </button>
                </td>
            </tr>
        </#list>
    </table>
    <!-- Group Rule editor dialog box -->
    <div class="modal" tabindex="-1" role="dialog" id="editGroupRule">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Edit Group Rule</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="group_rule_form" autocomplete="off">
                        <div class="form-group">
                            <div class="autocomplete">
                                <label for="class_name">Java Class Name</label>
                                <input type="text" class="form-control" name="class_name" id="class_name" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="group_name">Group Name</label>
                            <input type="text" class="form-control" name="group_name" id="group_name" />
                        </div>
                        <div class="form-group">
                            <label for="group_desc">Group Description</label>
                            <input type="text" class="form-control" name="group_desc" id="group_desc" />
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" name="after_hours" id="after_hours" />
                            <label for="after_hours" class="form-check-label">After Hours</label>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" name="run_alone" id="run_alone" />
                            <label for="run_alone" class="form-check-label">Run Alone</label>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" name="limit_removal" id="limit_removal" />
                            <label for="limit_removal" class="form-check-label">Limit Removal</label>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" name="allow_empty" id="allow_empty" />
                            <label for="allow_empty" class="form-check-label">Allow Empty</label>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" name="disabled" id="disabled" />
                            <label for="disabled" class="form-check-label">Disabled</label>
                        </div>
                        <div class="form-group mt-1">
                            <label for="owner_set_id">Owners</label>
                            <select class="form-control" name="owner_set_id" id="owner_set_id">
                                <option value="-1">None</option>
                                <#list owner_sets as row>
                                    <option value="${row.id}">${row.name}</option>
                                </#list>
                            </select>
                        </div>
                        <div id="extra_rule_data"></div>
                        <div class="my-1">
                            <button type="button" class="btn btn-success" onclick="add_new_rule_data();">Add rule data</button>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" onclick="do_save();">Save changes</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Delete Group Rule dialog box -->
    <div class="modal" tabindex="-1" role="dialog" id="deleteGroupRule">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Delete Group Rule</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="delete_group_rule_form">
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" name="can_delete" id="can_delete" />
                            <label for="can_delete" class="form-check-label"><span id="delete_group_rule_message"></span></label>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary" onclick="do_delete_group_rule();">Confirm</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Javascript functions for the modal dialog boxes above -->
    <script>
        autocomplete(document.getElementById("class_name"), ${rule_class_names!""});
        function do_edit_rule(record_id) {
            $.getJSON('/group/' + record_id, function( resp ) {
                $('#group_rule_form').attr('record_id', record_id);
                $('#extra_rule_data').empty().attr('sequence', 0);
                $('#owner_set_id').prop('selectedIndex', -1);
                $.each( resp['data'], function( key, value ) {
                    if (key === "limit_removal" || key === "after_hours" || key === "allow_empty" || key === "run_alone" || key === "disabled") {
                        $('[name="' + key + '"]').prop("checked", value === '1');
                    } else if (key === 'rule_data') {
                        value.forEach(add_rule_data);
                    } else {
                        $('[name="' + key + '"]').val(value);
                    }
                })
            });
            $('#editGroupRule').modal();
            $('#class_name').focus();
        }
        function add_rule_data(record, index) {
            let my_div = document.createElement('div');
            my_div.setAttribute('class', 'form-row my-2');
            let col1 = document.createElement('div');
            col1.setAttribute('class', 'col');
            let col2 = document.createElement('div');
            col2.setAttribute('class', 'col');
            let param_name = document.createElement('input');
            param_name.setAttribute('type', 'text');
            param_name.setAttribute('class', 'form-control');
            param_name.setAttribute('name', 'rule_data_name[' + record['id'] + ']');
            param_name.setAttribute('value', record['param_name']);
            let param_value = document.createElement('input');
            param_value.setAttribute('type', 'text');
            param_value.setAttribute('class', 'form-control');
            param_value.setAttribute('name', 'rule_data_value[' + record['id'] + ']');
            param_value.setAttribute('value', record['param_value']);
            col1.appendChild(param_name);
            col2.appendChild(param_value);
            my_div.appendChild(col1);
            my_div.appendChild(col2);
            $('#extra_rule_data').append(my_div);
        }
        function add_new_rule_data() {
            // I identify the new records by a descending sequence of negative numbers
            let my_seq = parseInt($('#extra_rule_data').attr('sequence')) - 1;
            add_rule_data({'id': my_seq, 'param_name': '', 'param_value': ''}, null);
            $('#extra_rule_data').attr('sequence', my_seq);
        }
        function do_save() {
            let record_id = $('#group_rule_form').attr('record_id');
            let form_values = $('#group_rule_form').serialize();
            $.ajax({
                type: 'POST',
                url: '/group/' + record_id,
                data: form_values
            })
                .done(function () {
                    $('#editGroupRule').modal('hide');
                    location.reload();
                })
                .fail(function (jqXHR, textStatus, error) {
                    let error_json = $.parseJSON(jqXHR.responseText)
                    let error_msg = error_json["message"];
                    $('#messagesArea').html('<div class="alert alert-danger flash mt-2" role="alert">Error saving record: ' + error_msg + '</div>');
                    $('#editGroupRule').modal('hide');
                })
        }
        function show_delete_group_rule_dialog(group_rule_id) {
            $('#can_delete').prop('checked', false);
            $('#delete_group_rule_message').text("Delete group rule #" + group_rule_id).attr('data-group-rule-id', group_rule_id);
            $('#deleteGroupRule').modal('show');
        }
        function do_delete_group_rule() {
            let group_rule_id = $('#delete_group_rule_message').attr('data-group-rule-id');
            let row_name = "#group_rule_row_" + group_rule_id;
            if ($('#can_delete').prop('checked')) {
                $('#deleteGroupRule').modal('hide');
                // console.log("[DEBUG] removing " + row_name);
                $.ajax({
                    type: 'DELETE',
                    url: '/group/' + group_rule_id,
                })
                    .done(function () {
                        $(row_name).empty();
                    })
                    .fail(function (jqXHR, textStatus, error) {
                        let error_json = $.parseJSON(jqXHR.responseText)
                        let error_msg = error_json["message"];
                        $('#messagesArea').html('<div class="alert alert-danger flash mt-2" role="alert">Error deleting record: ' + error_msg + '</div>');
                    });
            }
        }
    </script>
<#else>
    <div class="alert alert-danger flash" role="alert">
        <h4>Error</h4>
        <p>You must be logged in to access this page.</p>
    </div>
</#if>
