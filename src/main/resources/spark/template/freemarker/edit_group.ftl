<div class="row mt-4">
    <h4>AD Group Builder Rules</h4>
</div>
<#if logged_in??>
    <form id="edit_group_rule"></form>
    <script>

    </script>
<#else>
    <div class="alert alert-danger flash" role="alert">
        <h4>Error</h4>
        <p>You must be logged in to access this page.</p>
    </div>
</#if>
