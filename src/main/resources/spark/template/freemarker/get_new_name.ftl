<h2 class="mt-2">Rename User</h2>
<#if logged_in??>
    <#if securityLevel gte 3>
        <div class="container">
            <form action="/user/rename" method="post" class="add-entry">
                <dl>
                    <dt><label for="f_person_id">Employee ID Number:</label></dt>
                    <dd><input type="text" size="8" id="f_person_id" name="person_id" value="${person_id}" readonly/></dd>
                    <dt><label for="f_current_username">Current Username:</label></dt>
                    <dd><input type="text" size="20" id="f_current_username" name="current_username" value="${current_username}"
                               readonly/></dd>
                    <dt><label for="f_new_username">New Username:</label></dt>
                    <dd><input type="text" size="20" maxlength="20" id="f_new_username" name="new_username" autofocus
                               placeholder="${suggested_username}" required/></dd>
                </dl>
                <p><input type="submit" value="Rename" class="btn btn-lg btn-primary"/></p>
            </form>
        </div>
    <#else>
        <div class="alert alert-warning flash" role="alert">
            <h2>Warning</h2>
            <p>You are not authorized to access this page.</p>
        </div>
    </#if>
<#else>
    <div class="alert alert-danger flash" role="alert">
        <h2>Error</h2>
        <p>You must be logged in to access this page.</p>
    </div>
</#if>