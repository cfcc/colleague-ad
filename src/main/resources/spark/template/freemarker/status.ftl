<div class="row mt-4">
    <h4>Check AD User Status</h4>
</div>
<#if logged_in??>
    <div class="row">
        <div class="col-6">
            <form id="check_user" class="form-inline" method="post" onsubmit="checkUserStatus(); return false;">
                <div class="form-group">
                    <label for="person_id" class="my-1 mr-2">Colleague ID:</label>
                    <input type="text" placeholder="0000000" id="person_id" class="form-control" autofocus>
                </div>
                <button type="button" id="do_check_status" class="btn btn-primary ml-2 my-1">
                    <span id="check_text">Check</span>
                    <span id="check_spinner" class="spinner-border spinner-border-sm" role="status" aria-hidden="true" style="display: none">
                        <span class="sr-only">Loading...</span>
                    </span>
                </button>
            </form>
        </div>
        <div class="col-2">
            <span class="d-inline-block" tabindex="0" data-toggle="tooltip" title="Person Look-Up">
                <button type="button" id="btn_person_search" class="btn btn-success ml-2 my-1"><i class="fas fa-search"></i></button>
            </span>
        </div>
    </div>

    <div class="row">
        <h4 class="mt-2"></h4>
    </div>
    <div class="row">
        <div class="col-4">
            <div class="card">
                <div class="card-header bg-info" id="colleague_record_panel">
                    Colleague Record
                    <span class="d-inline-block" tabindex="0" data-toggle="tooltip" title="Record must exist in Colleague">
                        <i class="fas fa-question-circle"></i>
                    </span>
                </div>
                <div class="card-body"><p class="card-text" id="colleague_record"></p></div>
                <div class="d-none" id="whats-my-schedule-link">
                    <a href="#" id="show-schedule-anchor" class="btn btn-sm btn-outline-primary m-2" target="_blank">Show Schedule</a>
                </div>
            </div>
        </div>
        <div class="col-2">
            <img src="/images/Icon.blank-person.jpg" alt="Photo Missing" id="card_photo" class="img-fluid img-thumbnail">
        </div>

        <!-- Display the Update Status from the Envision Process Handler -->
        <div class="offset-1 col-4 d-none" id="next-run-time-div">
            <div class="card">
                <div class="card-header bg-light">
                    Next Scheduled Colleague <i class="fas fa-arrow-right"></i> AD Update
                </div>
                <div class="card-body">
                    <p class="card-text" id="next-run-time"></p>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <h4 class="mt-2">Applicant</h4>
    </div>
    <div class="row">
        <div class="col-3">
            <div class="card">
                <div class="card-header bg-info" id="appl_date_panel">
                    Application Date
                    <span class="d-inline-block" tabindex="0" data-toggle="tooltip" title="Account is created 2 days after the application date">
                        <i class="fas fa-question-circle"></i>
                    </span>
                </div>
                <div class="card-body"><p class="card-text" id="appl_date"></p></div>
            </div>
        </div>
        <div class="col-3">
            <div class="card">
                <div class="card-header bg-info" id="appl_status_panel">
                    Application Status
                    <span class="d-inline-block" tabindex="0" data-toggle="tooltip" title="Status must be 'Applied' or 'Moved to Student'">
                        <i class="fas fa-question-circle"></i>
                    </span>
                </div>
                <div class="card-body"><p class="card-text" id="appl_status"></p></div>
            </div>
        </div>
        <div class="col-1">
            <div class="d-none" id="applicant_arrow">
                <span style="color: green; "><i class="fas fa-2x fa-thumbs-up"></i></span>
            </div>
        </div>
        <div class="col-4 d-none" id="activedirectory_div">
            <div class="card">
                <div class="card-header bg-warning">
                    <i class="fas fa-exclamation-triangle"></i> Active Directory Sync
                </div>
                <div class="card-body">
                    <p class="card-text" id="activedirectory">
                        MESSAGE
                    </p>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <h4 class="mt-2">Student</h4>
    </div>
    <div class="row">
        <div class="col-3">
            <div class="card">
                <div class="card-header bg-info" id="stu_term_panel">
                    Student Term
                    <span class="d-inline-block" tabindex="0" data-toggle="tooltip" title="Must be enrolled in a current or future term">
                        <i class="fas fa-question-circle"></i>
                    </span>
                </div>
                <div class="card-body"><p class="card-text" id="stu_term"></p></div>
            </div>
        </div>
        <div class="col-3">
            <div class="card">
                <div class="card-header bg-info" id="stu_term_status_panel">
                    Term Status
                    <span class="d-inline-block" tabindex="0" data-toggle="tooltip" title="Term status must be 'Added' or 'New'">
                        <i class="fas fa-question-circle"></i>
                    </span>
                </div>
                <div class="card-body"><p class="card-text" id="stu_term_status"></p></div>
            </div>
        </div>
        <div class="col-1">
            <div class="d-none" id="student_arrow">
                <span style="color: green; "><i class="fas fa-2x fa-thumbs-up"></i></span>
            </div>
        </div>
        <div class="col-4 d-none" id="username_div">
            <div class="card">
                <div class="card-header bg-info" id="username_status_panel">
                    <i class="fas fa-check" id="username_status_icon"></i> User Account
                </div>
                <div class="card-body">
                    <p class="card-text" id="username">USERNAME</p>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <h4 class="mt-2">Employee</h4>
    </div>
    <div class="row">
        <div class="col-3">
            <div class="card">
                <div class="card-header bg-info" id="emp_pos_panel">
                    Active Position
                    <span class="d-inline-block" tabindex="0" data-toggle="tooltip" title="Must have an active position">
                        <i class="fas fa-question-circle"></i>
                    </span>
                </div>
                <div class="card-body"><p class="card-text" id="emp_pos"></p></div>
            </div>
        </div>
        <div class="col-3">
            <div class="card">
                <div class="card-header bg-info" id="emp_start_date_panel">
                    Start Date
                    <span class="d-inline-block" tabindex="0" data-toggle="tooltip" title="Account created up to 30 days before start date">
                        <i class="fas fa-question-circle"></i>
                    </span>
                </div>
                <div class="card-body"><p class="card-text" id="emp_start_date"></p></div>
            </div>
        </div>
        <div class="col-1">
            <div class="d-none" id="employee_arrow">
                <span style="color: green; "><i class="fas fa-2x fa-thumbs-up"></i></span>
            </div>
        </div>
        <div class="col-4 d-none" id="x2he_div">
            <div class="card">
                <div class="card-header bg-warning">
                    <i class="fas fa-exclamation-triangle"></i> Last Date Worked (X2HE)
                </div>
                <div class="card-body">
                    <p class="card-text" id="x2he">
                        MESSAGE
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="modal" tabindex="-1" role="dialog" id="waitDialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Searching...</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="text-center">
                        <div class="spinner-border" role="status">
                            <span class="sr-only">Loading...</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Person Search Modal Dialog -->
    <div class="modal" tabindex="-1" role="dialog" id="personSearchDialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Person Look-Up</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="personSearchForm">
                        <div class="form-row">
                            <div class="col">
                                <input type="text" class="form-control" id="first_name" name="first_name" placeholder="First Name">
                            </div>
                            <div class="col">
                                <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Last Name">
                            </div>
                        </div>
                    </form>
                    <p><small class="text-muted">The search uses a wildcard at the end, so you can put in partial names.</small></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" onclick="personSearch();">Search</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Select Person Dialog -->
    <div class="modal" tabindex="-1" role="dialog" id="selectPersonDialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Select a Person</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="person_list"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <script>
        $("#do_check_status").click(function(event) {
            event.preventDefault();
            checkUserStatus();
        });
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        });
        function checkUserStatus() {
            $("#check_text").hide();
            $("#check_spinner").show();
            /* clear out the Show Schedule form in case it was used before, and hide the button */
            $("#show-schedule-anchor").attr("href", "#");
            $("#whats-my-schedule-link").toggleClass("d-none", true);
            let url = "/check/" + $("#person_id").val();
            $.getJSON(url, function(result) {
                if (result.status === "SUCCESS") {
                    let data = result["data"];
                    if (data["colleague"]["import_complete"] === "true") {
                        $("#colleague_record_panel").attr("class", "card-header bg-success text-white");
                    } else {
                        $("#colleague_record_panel").attr("class", "card-header bg-danger text-white");
                    }
                    let photo_url = data["colleague"]["id_card_url"];
                    $("#card_photo").attr("src", photo_url);
                    $("#colleague_record").text(data["colleague"]["name"]);

                    if (data["applicant"]["app_date_complete"] === "true") {
                        $("#appl_date_panel").attr("class", "card-header bg-success text-white");
                    } else {
                        $("#appl_date_panel").attr("class", "card-header bg-danger text-white");
                    }
                    $("#appl_date").text(data["applicant"]["app_date"]);
                    if (data["applicant"]["app_status_complete"] === "true" && data["applicant"]["app_term_valid"] === "true") {
                        $("#appl_status_panel").attr("class", "card-header bg-success text-white");
                    } else {
                        $("#appl_status_panel").attr("class", "card-header bg-danger text-white");
                    }
                    $("#appl_status").empty().append(data["applicant"]["app_status"] + "<br>" + "Term: " + data["applicant"]["app_term"]);
                    if (data["applicant"]["app_date_complete"] === "true" && data["applicant"]["app_status_complete"] === "true" && data["applicant"]["app_term_valid"] === "true") {
                        $("#applicant_arrow").toggleClass("d-none", false);
                    } else {
                        $("#applicant_arrow").toggleClass("d-none", true);
                    }

                    if (data["student"]["term_is_current"] === "true") {
                        $("#stu_term_panel").attr("class", "card-header bg-success text-white");
                        /* Current students will also get a link to their schedule */
                        $("#show-schedule-anchor").attr("href", "https://www3.cfcc.edu/whats-my-schedule/index.php?studentid=" + data["colleague"]["id"]);
                        $("#whats-my-schedule-link").toggleClass("d-none", false);
                    } else {
                        /* CCCP students could have an expired term, but it will only be a warning */
                        if (data["student"]["term_status_complete"] === "true") {
                            $("#stu_term_panel").attr("class", "card-header bg-warning text-white");
                        } else {
                            $("#stu_term_panel").attr("class", "card-header bg-danger text-white");
                        }
                    }
                    $("#stu_term").text(data["student"]["term"]);
                    if (data["student"]["term_status_complete"] === "true") {
                        $("#stu_term_status_panel").attr("class", "card-header bg-success text-white");
                    } else {
                        $("#stu_term_status_panel").attr("class", "card-header bg-danger text-white");
                    }
                    $("#stu_term_status").text(data["student"]["term_status"]);
                    if (data["student"]["term_is_current"] === "true" && data["student"]["term_status_complete"] === "true") {
                        $("#student_arrow").toggleClass("d-none", false);
                    } else {
                        $("#student_arrow").toggleClass("d-none", true);
                    }

                    if (data["employee"]["active_position_complete"] === "true") {
                        $("#emp_pos_panel").attr("class", "card-header bg-success text-white");
                        $("#emp_start_date_panel").attr("class", "card-header bg-success text-white");
                    } else {
                        $("#emp_pos_panel").attr("class", "card-header bg-danger text-white");
                        $("#emp_start_date_panel").attr("class", "card-header bg-danger text-white");
                    }
                    $("#emp_pos").text(data["employee"]["active_position"]);
                    if (data["employee"]["start_date_future"] === "true") {
                        $("#emp_start_date_panel").attr("class", "card-header bg-warning");
                    }
                    $("#emp_start_date").text(data["employee"]["start_date"]);
                    if (data["employee"]["active_position_complete"] === "true") {
                        $("#employee_arrow").toggleClass("d-none", false);
                    } else {
                        $("#employee_arrow").toggleClass("d-none", true);
                    }

                    if (data["user"]["username_complete"] === "true") {
                        if (data["user"]["aduserstatus"] === "DISABLED") {
                            $("#username_status_panel").attr("class", "card-header bg-danger text-white");
                            $("#username_status_icon").attr("class", "fas fa-exclamation-circle")
                            $("#username").empty().append("<em>" + data["user"]["username"] + "</em><br>" + data["user"]["aduserstatusmsg"])
                        } else {
                            $("#username_status_panel").attr("class", "card-header bg-success text-white");
                            $("#username_status_icon").attr("class", "fas fa-check")
                            $("#username").text(data["user"]["username"]);
                        }
                        $("#username_div").toggleClass("d-none", false);
                    } else {
                        $("#username_div").toggleClass("d-none", true);
                    }

                    if (data["user"]["activedirectory"] === "SYNC-ERROR") {
                        $("#activedirectory").text(data["user"]["ad_sync_msg"]);
                        $("#activedirectory_div").toggleClass("d-none", false);
                    } else {
                        $("#activedirectory_div").toggleClass("d-none", true);
                    }
                    if (data["employee"]["x2he"] === "END-DATE-WARNING") {
                        $("#x2he").text(data["employee"]["x2he_msg"]);
                        $("#x2he_div").toggleClass("d-none", false);
                    } else {
                        $("#x2he_div").toggleClass("d-none", true);
                    }
                    if (data["eph"]["next_run_time"] !== "unknown") {
                        $("#next-run-time").text(data["eph"]["next_run_time"]);
                        $("#next-run-time-div").toggleClass("d-none", false);
                    } else {
                        $("#next-run-time-div").toggleClass("d-none", true);
                    }
                } else {
                    alert(result["message"]);
                }
                $("#check_text").show();
                $("#check_spinner").hide();
            });
        }
        $("#btn_person_search").click(function(event) {
            event.preventDefault();
            $('#last_name').val("");
            $('#first_name').val("");
            $('#personSearchDialog').modal();
        });
        $('#personSearchDialog').on('shown.bs.modal', function() {
            $('#first_name').focus();
        });
        function personSearch() {
            $('#personSearchDialog').modal('hide');
            $('#waitDialog').modal('show');
            let first_name = $('#first_name').val();
            let last_name = $('#last_name').val();
            let url = "/person/search?";
            if (last_name !== "") {
                url = url + "last=" + last_name;
            }
            if (last_name !== "" && first_name !== "") {
                url = url + "&";
            }
            if (first_name !== "") {
                url = url + "first=" + first_name;
            }
            $.getJSON(url, function(result) {
                if (result.status === "SUCCESS") {
                    $('#waitDialog').modal('hide');
                    let person_list = result['data'];
                    if (person_list.length === 0) {
                        alert('No records found');
                    } else {
                        if (person_list.length === 1) {
                            $('#person_id').val(person_list[0]['id']);
                            checkUserStatus();
                        } else {
                            $('#person_list').empty();
                            $.each(person_list, function (key, value) {
                                let row = document.createElement('div');
                                row.setAttribute('class', 'form-row my-1');

                                let col1 = document.createElement('div');
                                col1.setAttribute('class', 'col-2');
                                let p1 = document.createElement('p');
                                p1.innerText = value['id'];
                                col1.appendChild(p1);

                                let col2 = document.createElement('div');
                                col2.setAttribute('class', 'col-4');
                                let p2 = document.createElement('p');
                                p2.innerText = value['name'];
                                col2.appendChild(p2);

                                let col3 = document.createElement('div');
                                col3.setAttribute('class', 'col-4');
                                let p3 = document.createElement('p');
                                p3.innerText = value['where_used'];
                                col3.appendChild(p3);

                                let col4 = document.createElement('div');
                                col4.setAttribute('class', 'col-2');
                                let b1 = document.createElement('button');
                                b1.setAttribute('type', 'button');
                                b1.setAttribute('class', 'btn btn-success btn-sm float-right');
                                // b1.innerText = 'Select';
                                let icon1 = document.createElement('i');
                                icon1.setAttribute('class', 'fas fa-check');
                                icon1.onclick = function(){ personChoice(value['id']); };
                                b1.appendChild(icon1);
                                col4.appendChild(b1);
                                row.appendChild(col1);
                                row.appendChild(col2);
                                row.appendChild(col3);
                                row.appendChild(col4);
                                $('#person_list').append(row);
                            });
                            $('#selectPersonDialog').modal('show');
                        }
                    }
                } else {
                    $('#waitDialog').modal('hide');
                    alert(result.message);
                }
            });
        }
        function personChoice(person_id) {
            $('#selectPersonDialog').modal('hide');
            $('#person_id').val(person_id);
            checkUserStatus();
        }
    </script>
<#else>
    <div class="alert alert-danger flash" role="alert">
        <h4>Error</h4>
        <p>You must be logged in to access this page.</p>
    </div>
</#if>
