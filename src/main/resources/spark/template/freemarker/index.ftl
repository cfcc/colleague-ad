<div class="row">
    <div class="col-md-4">
        <h2>Rename Colleague User</h2>
        <p>Use this tool to change the Active Directory username as it is stored in Colleague.</p>
        <p><a class="btn btn-primary btn-lg" href="/user" role="button">Start &raquo;</a></p>
    </div>
    <div class="col-md-4">
        <h2>Check User Status</h2>
        <p>Use this tool to check the status of an AD user account created by Colleague.</p>
        <p><a class="btn btn-primary btn-lg" href="/status" role="button">Start &raquo;</a></p>
    </div>
    <div class="col-md-4">
        <h2>AD Group Builder Rules</h2>
        <p>Use this to to modify the rules for how Colleague/AD groups are built.</p>
        <p><a class="btn btn-primary btn-lg" href="/group" role="button">Start &raquo;</a></p>
    </div>
</div>
